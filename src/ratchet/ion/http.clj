;; Copyright (c) Cognitect, Inc.
;; All rights reserved.

(ns ratchet.ion.http
  (:require
    [clojure.java.io :as io]
    [ratchet.ion.lambdas :as ratchet]
    [clojure.edn :as edn]
    [clojure.data.json :as json]
    [ratchet.ion.crypto :as c]
    [datomic.ion.lambda.api-gateway :as apigw])
  (:import (com.amazonaws.util IOUtils)))

(def discord-config
  (let [r (io/resource "discord.edn")]
    (edn/read-string (slurp r))))

(defn edn-response
  [body]
  {:status 200
   :headers {"Content-Type" "application/edn"}
   :body body})

(defn json-response
  ([body]
   {:status 200
    :headers {"Content-Type" "application/json"}
    :body body})
  ([body status]
   {:status status
    :headers {"Content-Type" "application/json"}
    :body body}))

(defn wrap-response
  [r]
  {:type 4
   :data {:tts false
          :content r}})

(defn interact
  "Web handler that returns info about items matching type."
  [{:keys [headers body]}]
  (let [public-key (:public-key discord-config)
        signature (get headers "x-signature-ed25519")
        timestamp (get headers "x-signature-timestamp")
        msg (IOUtils/toString body)]
    (if (c/verify-signature public-key signature timestamp msg)
      (-> (json/read-str msg :key-fn keyword)
          ratchet/interact
          wrap-response
          json/write-str
          json-response)
      (-> {:error "Invalid Request Signature"}
          json/write-str
          (json-response 401)))))


(def interact-lambda-proxy
     (apigw/ionize interact))
