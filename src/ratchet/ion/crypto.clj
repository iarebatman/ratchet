(ns ratchet.ion.crypto
  (:import (org.bouncycastle.jce.provider BouncyCastleProvider))
  (:import (java.security Security KeyFactory Signature))
  (:import (org.bouncycastle.asn1.x509 AlgorithmIdentifier SubjectPublicKeyInfo)
           (java.security.spec X509EncodedKeySpec)
           (org.bouncycastle.asn1.edec EdECObjectIdentifiers))
  (:require [ratchet.ion.utils :as u]))

(def bc-provider (BouncyCastleProvider.))
(Security/addProvider bc-provider)

(defn verify-signature
      [pk sig ts msg]
      (let [s (u/hex->bytes sig)
            bk (u/hex->bytes pk)
            algi (AlgorithmIdentifier. EdECObjectIdentifiers/id_Ed25519)
            pki (SubjectPublicKeyInfo. algi bk)
            pks (X509EncodedKeySpec. (.getEncoded pki))
            kf (KeyFactory/getInstance "ed25519", bc-provider)
            pk (.generatePublic kf pks)
            sv (Signature/getInstance "ed25519" bc-provider)]
        (do
          (.initVerify sv pk)
          (.update sv (.getBytes ts))
          (.update sv (.getBytes msg))
          (.verify sv s))))