(ns ratchet.ion.lambdas
    (:require
      [clojure.edn :as edn]
      [clojure.java.io :as io]
      [datomic.client.api :as d]
      [ratchet.ion.utils :as utils]))

(def database-name "ratchet")

(def get-client
  "Return a shared client.
  before calling this function."
  (memoize #(if-let [r (io/resource "datomic/ion/ratchet/config.edn")]
                    (d/client (edn/read-string (slurp r)))
                    (throw (RuntimeException. "You need to add a resource datomic/ion/ratchet/config.edn with your connection config")))))

;; (d/create-database (get-client) {:db-name database-name})

(defn get-connection
      "Get shared connection."
      []
      (utils/with-retry #(d/connect (get-client) {:db-name database-name})))
;
(defn get-db
  []
  (d/db (get-connection)))

;(def ratchet-schema
;  [{:db/ident :quote/quote
;    :db/valueType :db.type/string
;    :db/cardinality :db.cardinality/one}
;   {:db/ident :quote/source
;    :db/valueType :db.type/string
;    :db/cardinality :db.cardinality/one}])

;; (d/transact (get-connection) {:tx-data ratchet-schema})

;(def sample-quotes [{:quote/source "Jean-Luc Picard"
;                     :quote/quote "If you'd earned that uniform you're wearing you'd know that the unknown is what brings us out here."}
;                    {:quote/source "Jean-Luc Picard"
;                     :quote/quote "I don't know how to communicate this, or even if it is possible, but the question of justice has concerned me greatly of late and I say to any creature who may be listening, there can be no justice so long as laws are absolute. Even life itself is an exercise in exceptions."}])

;; (d/transact (get-connection) {:tx-data sample-quotes})

(defn get-quote
  "Retrieves a random quote"
  []
  (let [db (get-db)
        [eid] (ffirst
                (d/q '[:find (rand 1 ?e)
                       :where [?e :quote/quote]]
                    db))]
    (d/pull db '[:quote/quote :quote/source] eid)))


(defn format-quote
  [q]
  (str (:quote/quote q) "\n  --" (:quote/source q)))

(defn interact
  "Invoked by Discord"
  [body]
  (cond
    (= (:type body) 1) {:type 1}
    (= (get-in body [:data :name]) "quote") (-> (get-quote)
                                                format-quote)))
(comment
  (def quotes
    [{:quote/quote "Thou art notified that thy kind has infiltrated the galaxy too far already. Thou art directed to return to thine own solar system immediately." :quote/source "Q"}
     {:quote/quote "Go back, or thou shalt most certainly die!" :quote/source "Q"}
     {:quote/quote "But you can't deny that you're still a dangerous, savage child race." :quote/source "Q"}
     {:quote/quote "...and 400 years before that, you were murdering each other in quarrels over tribal god images.  Since then, there are no indications that humans will ever change." :quote/source "Q"}
     {:quote/quote "There are preparations to make, but when we next meet, Captain, we'll proceed exactly as you suggest." :quote/source "Q"}
     {:quote/quote "If we're going to be damned, let's be damned for what we really are." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I am superior, sir, in many ways, but I would gladly give it up to be human." :quote/source "Data"}
     {:quote/quote "Nice to meet you... Pinocchio." :quote/source "William Riker"}
     {:quote/quote "A joke." :quote/source "William Riker"}
     {:quote/quote "Ah! Intriguing." :quote/source "Data"}
     {:quote/quote "You're going to be an interesting companion, Mr. Data." :quote/source "William Riker"}
     {:quote/quote "It is an unknown, Captain.  Isn't that enough?" :quote/source "Q"}
     {:quote/quote "If you'd earned that uniform you're wearing you'd know that the unknown is what brings us out here." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Have you got some of a reason my atoms scattered all over space, boy?" :quote/source "Admiral McCoy"}
     {:quote/quote "No, sir. But at your age, sir, I thought you shouldn't have to put up with the time, and trouble of a shuttlecraft." :quote/source "Data"}
     {:quote/quote "Hold it right there, boy?" :quote/source "Admiral McCoy"}
     {:quote/quote "Sir?" :quote/source "Data"}
     {:quote/quote "What about my age?" :quote/source "Admiral McCoy"}
     {:quote/quote "Sorry, sir. If that subject troubles you..." :quote/source "Data"}
     {:quote/quote "Troubles me? What's so damn troublesome about not havin' died? How old you think I am anyway?" :quote/source "Admiral McCoy"}
     {:quote/quote "137 years, Admiral, according to Starfleet records." :quote/source "Data"}
     {:quote/quote "Explain how you remembered that so exactly." :quote/source "Admiral McCoy"}
     {:quote/quote "I remember every fact I am exposed to, sir." :quote/source "Data"}
     {:quote/quote "I don't see no points on your ears boy, but you sound like a Vulcan." :quote/source "Admiral McCoy"}
     {:quote/quote "No, sir, I am an android." :quote/source "Data"}
     {:quote/quote "Almost as bad." :quote/source "Admiral McCoy"}
     {:quote/quote "I thought it was generally accepted, sir, that Vulcans are an advanced, and most honorable race." :quote/source "Data"}
     {:quote/quote "They are, they are, and damn annoying at times." :quote/source "Admiral McCoy"}
     {:quote/quote "Yes, sir." :quote/source "Data"}
     {:quote/quote "Well, this is a new ship, but she's got the right name. Now you remember that you hear?" :quote/source "Admiral McCoy"}
     {:quote/quote "I will sir." :quote/source "Data"}
     {:quote/quote "You treat her like a lady, and she'll always bring you home." :quote/source "Admiral McCoy"}
     {:quote/quote "Indications of what humans would call... a wild party." :quote/source "Data"}
     {:quote/quote '"There was a young lady from Venus, whose body was shaped like a...'' :quote/source 'Data"}
     {:quote/quote "[frantically cutting off Data] Captain to Security, come in!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Did I say something wrong?" :quote/source "Data"}
     {:quote/quote "I don't understand their humor, either." :quote/source "Worf"}
     {:quote/quote "Data. You are fully functional, aren't you?" :quote/source "Tasha Yar"}
     {:quote/quote "[intoxicated by the virus] We are more alike than unlike, my dear Captain. I have pores. Humans have pores. I have... fingerprints. Humans have fingerprints. My chemical nutrients are like your blood. If you prick me... do I not... leak?" :quote/source "Data"}
     {:quote/quote "Where are the calluses we doctors are supposed to grow over our feelings?" :quote/source "Beverly Crusher"}
     {:quote/quote "Perhaps the good ones never get them." :quote/source "Jean-Luc Picard"}
     {:quote/quote "For example, what Lutan did is similar to what certain American Indians once did called 'counting coup.'  That is from an obscure language known as French.  'Counting coup...'' :quote/source 'Data"}
     {:quote/quote "Mr. Data, the French language for centuries on Earth represented civilization." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Indeed?  But surely, sir..." :quote/source "Data"}
     {:quote/quote "I suggest you drop it, Mr. Data." :quote/source "William Riker"}
     {:quote/quote "Yes, sir.  'Counting coup' could be something as simple as touching an enemy with a stick in battle or taking something from him and escaping.' :quote/source 'Data"}
     {:quote/quote "You speak of a code of honor, but what you are saying now, according to our customs, is called an act of war." :quote/source "Jean-Luc Picard"}
     {:quote/quote "This is not an act of war, but of love! I want Lieutenant Yar to become my first one." :quote/source "Lutan"}
     {:quote/quote "I challenge your right of supersedence!" :quote/source "Yareena"}
     {:quote/quote "No woman has challenged supersedence for over 200 years!" :quote/source "Hagon"}
     {:quote/quote "The right is mine, and I will have it. Natasha Yar, I challenge you to a struggle to the death." :quote/source "Yareena"}
     {:quote/quote "No! The challenge is unequivocally refused." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Then you shall have no treaty! No vaccine! And no Lieutenant Yar!" :quote/source "Lutan"}
     {:quote/quote "[his fingers in a Chinese finger trap] Apologies, Captain. I seem to have reached an odd functional impasse. I am, uh... stuck." :quote/source "Data"}
     {:quote/quote "Then get unstuck and continue with the briefing." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Yes, sir. That is what I'm trying to do, sir, but the solution eludes me." :quote/source "Data"}
     {:quote/quote "My hero." :quote/source "Geordi La Forge"}
     {:quote/quote '"Fear is the true enemy, the only enemy.' [From Sun Tzu]' :quote/source 'William Riker"}
     {:quote/quote "...where is this place?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Where none have gone before." :quote/source "Data"}
     {:quote/quote "If the Enterprise were really this fragile, sir, she never would have left spacedock. Therefore, her systems' failures are not endemic to the ship, but are the result of the actions of an unknown adversary." :quote/source "Data"}
     {:quote/quote "We have a saboteur aboard." :quote/source "William Riker"}
     {:quote/quote "I believe I said that." :quote/source "Data"}
     {:quote/quote "It's elementary, my dear Riker. Sir." :quote/source "Data"}
     {:quote/quote '"We must fall back on the old axiom that when other contingencies fail, whatever remains, however improbable, must be the truth.'' :quote/source 'Data"}
     {:quote/quote "[booming voice] State... the... purpose. State the purpose of what you have done." :quote/source "Edo God"}
     {:quote/quote "I'm Captain Picard, commanding this Federation starship." :quote/source "Jean-Luc Picard"}
     {:quote/quote "State the purpose of your visit here." :quote/source "Edo God"}
     {:quote/quote "We have sent down what we call an away team, to make peaceful contact here." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Do you plan to leave life-forms here?" :quote/source "Edo God"}
     {:quote/quote "No. We are merely visiting here." :quote/source "Jean-Luc Picard"}
     {:quote/quote "But you did more at the world you just left. Why have you left your own life-forms there?" :quote/source "Edo God"}
     {:quote/quote "The colony we just planted, sir." :quote/source "Data"}
     {:quote/quote "We found that world uninhabited. The life-forms we left there had... had sought the challenge-- at least, that is the basic reason-- had sought the challenge of creating a new lifestyle-- a new society there. Life on our world is driven to protect itself by seeding itself as widely as possible." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Do not interfere with my children below." :quote/source "Edo God"}
     {:quote/quote "They are called mediators and they are needed only in one place each day." :quote/source "Rivan"}
     {:quote/quote "The punishment zone, an area that's selected for a period of time." :quote/source "Liator"}
     {:quote/quote "It's a completely random selection?" :quote/source "Tasha Yar"}
     {:quote/quote "No one but our mediators know what place or for how long. We're very proud of the wisdom of our ancestors. No person ever knows where or when a zone will be." :quote/source "Liator"}
     {:quote/quote "And so no one risks death." :quote/source "Rivan"}
     {:quote/quote "Death?" :quote/source "Worf"}
     {:quote/quote "By breaking any law." :quote/source "Rivan"}
     {:quote/quote "Wait, explain this." :quote/source "Tasha Yar"}
     {:quote/quote "Only one punishment for any crime." :quote/source "Liator"}
     {:quote/quote "Anyone who commits any crime in the punishment zone dies?" :quote/source "Worf"}
     {:quote/quote "So, we are not yet as advanced as they are. And since you are advanced in other ways, too, I suggest you use your superior powers to rescue the Wesley boy. We will record him as a convicted criminal out of our reach-- an advanced person who luckily escaped the barbarism of this backward little world." :quote/source "Liator"}
     {:quote/quote "They were able to communicate with me quite... I was about to say quite easily but there was nothing easy about it. Fortunately, they stopped short of overloading my circuitry." :quote/source "Data"}
     {:quote/quote "You're saying they? So it is a vessel of some sort?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Definitely not a single entity, if that's what you mean, sir, although they know the Edo worship them as a god thing." :quote/source "Data"}
     {:quote/quote "They know?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "They recognize that this is quite expected and harmless at the present Edo stage of evolution." :quote/source "Data"}
     {:quote/quote "What sort of vessel?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "It is perhaps not what we would understand as a vessel, sir. The dimensions this one occupies allows them to be... well, to be in several places at once, that they consider that this entire star cluster is theirs. It was probably unwise of us to attempt to place a human colony in this area. Of course, there are 3,004 other planets in this star cluster in which we could have colonized. The largest and closest..." :quote/source "Data"}
     {:quote/quote "Data! Don't babble." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Babble, sir? I am not aware that I ever babble, sir. It may be that from time to time I have considerable information to communicate, and you may question the way in which I organize it..." :quote/source "Data"}
     {:quote/quote "Please, organize it into brief answers to my questions. We have very little time. Do they accept our presence at this planet?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Undecided, sir." :quote/source "Data"}
     {:quote/quote "Data, please feel free to volunteer any important information." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I volunteer that they are now observing us, sir." :quote/source "Data"}
     {:quote/quote "To judge what kind of life-forms we are?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "No. It is more curiosity. I doubt that they expect us to abide by their value systems." :quote/source "Data"}
     {:quote/quote "Do they know of our Prime Directive?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "They know everything I know, sir." :quote/source "Data"}
     {:quote/quote "And if we were to violate the Prime Directive..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "That's not a fair question." :quote/source "Beverly Crusher"}
     {:quote/quote "How would they react?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "That would be a case of judging us by our own rules, sir. If we violate our own Prime Directive they might consider us to be deceitful and untrustworthy. You do recall, they cautioned us not to interfere with their children below. What has happened?" :quote/source "Data"}
     {:quote/quote "The Edo want to execute my son." :quote/source "Beverly Crusher"}
     {:quote/quote "What of Justice to Wesley? Does he deserve to die?" :quote/source "Tasha Yar"}
     {:quote/quote "I'm truly sorry, Liator, but I must have justice for my people, too. Transporter Room, energize. Transporter Room, come in." :quote/source "Jean-Luc Picard"}
     {:quote/quote "We can't energize the beam, sir. Everything checks out, but we're getting no results." :quote/source "Transporter Officer"}
     {:quote/quote "God has prevented your escape." :quote/source "First Mediator"}
     {:quote/quote "Then your God is unfair. My son had no warning that his act was criminal." :quote/source "Beverly Crusher"}
     {:quote/quote "We cannot allow ignorance of the law to become a defense." :quote/source "Second Mediator"}
     {:quote/quote "I don't know how to communicate this, or even if it is possible, but the question of justice has concerned me greatly of late and I say to any creature who may be listening, there can be no justice so long as laws are absolute. Even life itself is an exercise in exceptions." :quote/source "Jean-Luc Picard"}
     {:quote/quote "When has justice ever been as simple as a rule book?" :quote/source "William Riker"}
     {:quote/quote "It is a gift from us with which we honor the hero of Maxia." :quote/source "Bok"}
     {:quote/quote "Your shields were failing, sir." :quote/source "William Riker"}
     {:quote/quote "Mm-hmm. I... improvised. With the enemy vessel coming in for the kill, I ordered a sensor bearing, and when it came into the return arc..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "You performed what Starfleet textbooks now refer to as the 'Picard Maneuver.'' :quote/source 'Data"}
     {:quote/quote "Well, I did what any good helmsman would have done. I dropped into high warp, stopped right off the enemy vessel's bow fired everything I had." :quote/source "Jean-Luc Picard"}
     {:quote/quote "And blowing into maximum warp speed, you appeared for an instant to be in two places at once." :quote/source "William Riker"}
     {:quote/quote "And our attacker fired on the wrong one." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Collecting on an old debt." :quote/source "Bok"}
     {:quote/quote "Die well, Captain!" :quote/source "Bok"}
     {:quote/quote "First Officer Kazago to human Riker." :quote/source "Kazago"}
     {:quote/quote "Not now, Kazago!" :quote/source "William Riker"}
     {:quote/quote "We do not wish to become involved in what is clearly a Federation matter..." :quote/source "Kazago"}
     {:quote/quote "Fine, fine! Enterprise, out." :quote/source "William Riker"}
     {:quote/quote "You should also know that DaiMon Bok no longer commands this vessel. His first officer has confined him for engaging in this unprofitable adventure. Good luck, First Officer Riker." :quote/source "Kazago"}
     {:quote/quote "Bok! Where is Bok?!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Removed from command, sir. And placed under guard for his act of personal vengeance. Seems there was no profit in it." :quote/source "William Riker"}
     {:quote/quote "In revenge, there never is. Let the dead rest... and the past, remain the past. Enterprise, lock on. Beam me home, Riker." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Oh, I know 'Hamlet.' And what he might say with irony I say with conviction. 'What a piece of work is man! How noble in reason. How infinite in faculty. In form, in moving, how express and admirable. In action, how like an angel. In apprehension, how like a god.'' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "Surely you don't see your species like that, do you?" :quote/source "Q"}
     {:quote/quote "I see us becoming that, Q. Is it that which concerns you?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "After the young couple have removed their clothing..." :quote/source "Lwaxana Troi"}
     {:quote/quote "The bride and groom go naked?" :quote/source "Tasha Yar"}
     {:quote/quote "Could you please continue the petty bickering? I find it most intriguing." :quote/source "Data"}
     {:quote/quote "Earth, United States, San Francisco, California." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Time period?" :quote/source "Computer"}
     {:quote/quote "1941 AD." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Hiya, Doc. What's cooking?" :quote/source "Data"}
     {:quote/quote "You know I had some trouble getting through? Where's Captain Picard?" :quote/source "Beverly Crusher"}
     {:quote/quote "He's on ice." :quote/source "Data"}
     {:quote/quote "Pardon?" :quote/source "Beverly Crusher"}
     {:quote/quote "He's being grilled." :quote/source "Data"}
     {:quote/quote "What is he, a fish?" :quote/source "Beverly Crusher"}
     {:quote/quote "Kill the woman." :quote/source "Cyrus Redblock"}
     {:quote/quote "So... this is the big good-bye." :quote/source "McNary"}
     {:quote/quote "It was raining in the City by the Bay-- a hard rain. Hard enough to wash the slime..." :quote/source "Data"}
     {:quote/quote "Data!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Sorry, sir." :quote/source "Data"}
     {:quote/quote "If you had an off switch, Doctor, would you not keep it secret?" :quote/source "Data"}
     {:quote/quote "I guess I would." :quote/source "Beverly Crusher"}
     {:quote/quote "You can't rescue a man from the place he calls his home." :quote/source "Ramsey"}
     {:quote/quote "If winning is not important, then Commander, why keep score?" :quote/source "Worf"}
     {:quote/quote "Keep notes. This project might turn out to be of interest to future scholars." :quote/source "William T. Riker"}
     {:quote/quote "Really?" :quote/source "Geordi La Forge"}
     {:quote/quote "Think about it. A blind man teaching an android to paint? That's got to be worth a couple of pages in someone's book." :quote/source "William T. Riker"}
     {:quote/quote "What's a knock-out like you doing in a computer-generated gin joint like this?" :quote/source "William T. Riker"}
     {:quote/quote "Waiting for you." :quote/source "Minuet"}
     {:quote/quote "Doesn't love always begin that way? With the illusion being more real than the woman? ." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Age and wisdom have their graces." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I wonder if one doesn't have to have age and wisdom to appreciate that, sir." :quote/source "William T. Riker"}
     {:quote/quote "I hope not, Number One. It would be such a waste of youth." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Data, find a way to defeat that shield." :quote/source "Jean-Luc Picard"}
     {:quote/quote "That may be impossible, sir." :quote/source "Data"}
     {:quote/quote "Data, things are only impossible until they're not." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Ugly giant bags... of mostly water..." :quote/source "Microbrain"}
     {:quote/quote "Do Klingons observe birthdays, Worf?" :quote/source "Data"}
     {:quote/quote "Klingons are born, live as warriors, and die." :quote/source "Worf"}
     {:quote/quote "Then how do you know how old you are?" :quote/source "Data"}
     {:quote/quote "I don't. Do you know?" :quote/source "Worf"}
     {:quote/quote "I have no age." :quote/source "Data"}
     {:quote/quote "Can you filter out the extraneous information?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "No, I get it all simultaneously." :quote/source "Geordi La Forge"}
     {:quote/quote "It's a jumble. How can you make heads or tails of it?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I select what I want to see and disregard the rest." :quote/source "Geordi La Forge"}
     {:quote/quote "How is that possible?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "In a noisy room, how can you pick out one specific voice or sound?" :quote/source "Geordi La Forge"}
     {:quote/quote "I see. Something you learn." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Why do you mock me? Why do you wish to anger me?" :quote/source "Worf"}
     {:quote/quote "Only to see if it is still possible." :quote/source "Korris"}
     {:quote/quote "It is." :quote/source "Worf"}
     {:quote/quote "My brother, it is you who does not see. You look for battles in the wrong place. The true test of a warrior is not without, it is within! Here! Here is where we meet the challenge. It is the weaknesses in here a warrior must overcome." :quote/source "Worf"}
     {:quote/quote "No." :quote/source "Korris"}
     {:quote/quote "You have talked of glory, and of conquest, and of legends that we will write." :quote/source "Worf"}
     {:quote/quote "Yes! The birthright of every Klingon." :quote/source "Korris"}
     {:quote/quote "Yet, for all you say, where are the words 'duty,' 'honor' and 'loyalty'? Without which a warrior is nothing!' :quote/source 'Worf"}
     {:quote/quote "What are you saying?! Living among these humans has sucked the Klingon heart out of you." :quote/source "Korris"}
     {:quote/quote "Put down the phaser." :quote/source "Worf"}
     {:quote/quote "You are a sham! My words were as dust upon the ground. Your blood has no fire! You are weak like them! I don't care what you look like! YOU ARE NO KLINGON!" :quote/source "Korris"}
     {:quote/quote "Perhaps not. [shoots Korris dead]" :quote/source "Worf"}
     {:quote/quote "Whoever you are, wherever you're from, greetings! Welcome to Minos, The Arsenal of Freedom!" :quote/source "The Peddler"}
     {:quote/quote "I'm Captain Jean-Luc Picard of the U.S.S. Enterp..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "If you need a little something 'special', be it for one target or multiple targets, we've got it, you'll see it, here on Minos, where we live by the motto, 'Peace through superior firepower.'' :quote/source 'The Peddler"}
     {:quote/quote "To whom am I speaking...?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "To be totally armed is to be totally secure! Remember, the early bird that hesitates gets wormed." :quote/source "The Peddler"}
     {:quote/quote "It is a recorded message, sir." :quote/source "Data"}
     {:quote/quote "perfection in highly advanced weaponry. Versatility, flexibility, and everything 100% guaranteed! So lock on to my signal and beam on down! Because we don't just provide weapons..." :quote/source "The Peddler: Minos, the Arsenal of Freedom"}
     {:quote/quote "Shut that off." :quote/source "Jean-Luc Picard"}
     {:quote/quote "We provide complete weapons sys-" :quote/source "The Peddler"}
     {:quote/quote "That's a heck of a sales pitch." :quote/source "Geordi La Forge"}
     {:quote/quote "Tell me about your ship - the Enterprise, isn't it?" :quote/source "Paul Rice"}
     {:quote/quote "No... The name of my ship is the Lollipop." :quote/source "William T. Riker"}
     {:quote/quote "I have no knowledge of that ship." :quote/source "Paul Rice"}
     {:quote/quote "It's just been commissioned. It's a good ship." :quote/source "William T. Riker"}
     {:quote/quote "Relinquishing command, Captain." :quote/source "Geordi La Forge"}
     {:quote/quote "As you were, Lieutenant!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Sir?" :quote/source "Geordi La Forge"}
     {:quote/quote "Mr. La Forge, when I left this ship, it was in one piece.  I would appreciate your returning it to me in the same condition.  Do you concur, Number One?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Absolutely, sir." :quote/source "William Riker"}
     {:quote/quote "Everything's just kind of... you know... dead, I guess. It's all... you know... shut down." :quote/source "T'Jon"}
     {:quote/quote "That's a little vague. What's the computer analysis?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Uh, the computer's not working very well." :quote/source "T'Jon"}
     {:quote/quote "The Prime Directive is not just a set of rules; it is a philosophy... and a very correct one. History has proven again and again that whenever mankind interferes with a less developed civilization, no matter how well intentioned that interference may be, the results are invariably disastrous." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I'm Commander William Riker of the USS Enterprise." :quote/source "William Riker"}
     {:quote/quote "I am Armus. Why are you here?" :quote/source "Armus"}
     {:quote/quote "We mean you no harm. We have injured crewmen in the shuttlecraft, and we need to get to them. May we pass?" :quote/source "William Riker"}
     {:quote/quote "You haven't given me a good enough reason." :quote/source "Armus"}
     {:quote/quote "Preserving life. All life is important to us." :quote/source "William Riker"}
     {:quote/quote "Why?" :quote/source "Armus"}
     {:quote/quote "We believe everything in the universe has a right to exist." :quote/source "William Riker"}
     {:quote/quote "An interesting notion. Which I do not share! You may leave now, if you wish." :quote/source "Armus"}
     {:quote/quote "We're not going anywhere without our shuttle crew." :quote/source "Tasha Yar"}
     {:quote/quote "I warn you..." :quote/source "Armus"}
     {:quote/quote "Enough! We have people who need attention. We won't hurt you, but we must help them." :quote/source "Tasha Yar"}
     {:quote/quote "Death is that state where one lives only in the memory of others, which is why it is not an end. No goodbyes—just good memories." :quote/source "Tasha Yar"}
     {:quote/quote " Au revoir, Natasha. The gathering is concluded." :quote/source "Picard"}
     {:quote/quote "Sir, the purpose of this gathering...confuses me." :quote/source "Data"}
     {:quote/quote "Oh? How so?" :quote/source "Picard"}
     {:quote/quote "My thoughts are not for Tasha, but for myself. I keep thinking how empty it will feel without her presence. Did I miss the point?" :quote/source "Data"}
     {:quote/quote "No, you didn't, Data. You got it." :quote/source "Picard"}
     {:quote/quote "(fencing with Picard) Interesting move. What technique was that?" :quote/source "Unnamed Officer"}
     {:quote/quote "The technique of a desperate man." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Well, so much for my dramatic exit." :quote/source "Jenice Manheim"}
     {:quote/quote "You are aware, Counselor, that the holodeck can be programmed to recreate an oceanic environment?" :quote/source "Data"}
     {:quote/quote "Data, it's just not the same. Have you ever gone for a real moonlight swim?" :quote/source "Deanna Troi"}
     {:quote/quote "One can swim in moonlight?" :quote/source "Data"}
     {:quote/quote "Are you okay?" :quote/source "Worf"}
     {:quote/quote "If I could see, I'd be seeing stars now." :quote/source "Geordi La Forge"}
     {:quote/quote "(calm, sincere) You don't understand. (voice distorted) We mean you no harm. We seek peaceful co-existence." :quote/source "Parasite mother creature/Dexter Remmick"}
     {:quote/quote "...you and me can find us a couple of low-mileage pit wolfies, and help 'em build a memory.' :quote/source ''Sonny' Clemonds"}
     {:quote/quote "...you're just about the prettiest lil' old doctor I've ever seen.' :quote/source ''Sonny' Clemonds"}
     {:quote/quote "Our lives just became a lot more complicated." :quote/source "Picard"}
     {:quote/quote "Captain. do what you must to protect the ship. But know this, I am going to have this baby." :quote/source "Deanna"}
     {:quote/quote "Dah-ta, look at this." :quote/source "Kate Pulaski"}
     {:quote/quote "[looking slightly confused] 'Day-ta'." :quote/source "Data"}
     {:quote/quote "What?" :quote/source "Kate Pulaski"}
     {:quote/quote "My name. It is pronounced 'Day-ta'." :quote/source "Data"}
     {:quote/quote "Oh?" :quote/source "Kate Pulaski"}
     {:quote/quote "You called me 'Dah-ta'." :quote/source "Data"}
     {:quote/quote "[laughing] What's the difference?" :quote/source "Kate Pulaski"}
     {:quote/quote "One is my name. The other is not." :quote/source "Data"}
     {:quote/quote "What is death?" :quote/source "Alien (as Data)"}
     {:quote/quote "Oh, is that all. Data, you're asking probably the most difficult of all questions. Some see it as a changing into an indestructible form, forever unchanging. They believe that the purpose of the entire universe is to then maintain that form in an Earth-like garden which will give delight and pleasure through all eternity. On the other hand there are those who hold to the idea of our blinking into nothingness with all of our experiences and hopes and dreams merely a delusion." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Which do you believe sir?" :quote/source "Alien (as Data)"}
     {:quote/quote "Considering the marvelous complexity of the universe, its clockwork perfection, its balances of this against that... matter, energy, gravitation, time, dimension, I believe that our existence must be more than either of these philosophies. That what we are goes beyond euclidean or other 'practical' measuring systems... and that our existence is part of a reality beyond what we understand now as reality." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Ten seconds to auto-destruct." :quote/source "Computer"}
     {:quote/quote "Captain?" :quote/source "William T. Riker"}
     {:quote/quote "Abort auto-destruct sequence." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Riker, William T. Do you concur?" :quote/source "Computer"}
     {:quote/quote "Yes, absolutely I do indeed concur, wholeheartedly!" :quote/source "William T. Riker"}
     {:quote/quote "Auto-destruct cancelled." :quote/source "Computer"}
     {:quote/quote "A simple 'yes' would have sufficed Number One.' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "I didn't want there to be any chance of a misunderstanding." :quote/source "William T. Riker"}
     {:quote/quote "All right, Data.  You solve all the cases and get all the gifts, but what do I do?" :quote/source "Geordi La Forge"}
     {:quote/quote "Primarily as Dr. Watson, you will keep a written record of everything I say and do." :quote/source "Data"}
     {:quote/quote "It has gone beyond that little game, Mr. Data.  And you'll note I no longer call you Holmes." :quote/source "Moriarty"}
     {:quote/quote "I'll give you credit for your vast knowledge but your circuits would just short out if you were confronted with a truly original mystery." :quote/source "Pulaski"}
     {:quote/quote "Your artificial friend doesn't have a prayer of solving a Holmes mystery that he hasn't read." :quote/source "Pulaski"}
     {:quote/quote "That dark fellow there used the word 'arch' and then, I wonder… arch!' :quote/source 'Moriarty"}
     {:quote/quote "Which you asked the computer to provide?" :quote/source "Picard"}
     {:quote/quote "Yes, with a worthy opponent." :quote/source "La Forge"}
     {:quote/quote "Worthy of Holmes?" :quote/source "Picard"}
     {:quote/quote "...Oh my God...I asked for a Holmes-type mystery with an opponent capable of defeating...Data! That's gotta be it!" :quote/source "La Forge"}
     {:quote/quote "Merde..." :quote/source "Picard"}
     {:quote/quote "(to Okona) Now please, follow Cmdr. Rikers' commands so our ship can go back to its normal routine." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Something funny?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Well, the unexpected is our normal routine." :quote/source "William T. Riker"}
     {:quote/quote "So... if you put funny teeth in your mouth, and jump around like an idiot... that is considered funny!" :quote/source "Data"}
     {:quote/quote "Say goodbye, Data." :quote/source "Wesley Crusher"}
     {:quote/quote "Goodbye, Data." :quote/source "Data"}
     {:quote/quote "Was that funny?" :quote/source "Data"}
     {:quote/quote "Accessing... Ah, Burns & Allen, Roxy Theatre, New York City, 1932. It still works!" :quote/source "Data"}
     {:quote/quote "When I stroke the beard thusly, do I not appear more intellectual?" :quote/source "Data"}
     {:quote/quote "No being is so important that he can usurp the rights of another." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Chief medical officer's log, supplemental...Scientists believe no experiment is a failure, that even a mistake advances the evolution of understanding. But all achievement has a price. For one brief glance at the mysterious blueprint of human evolution, the men and women of the U.S.S. Lantree paid with their lives. Their sacrifice is thus noted in this scientist's log." :quote/source "Kate Pulaski"}
     {:quote/quote "I've studied, and know everything about my heritage." :quote/source "Worf"}
     {:quote/quote "Then you're just the person I need to talk to clear something up." :quote/source "William Riker"}
     {:quote/quote "It's been my understanding that one of the duties of the First Officer of the Klingon vessel is to assassinate his Captain?" :quote/source "William Riker"}
     {:quote/quote "Yes, sir." :quote/source "Worf"}
     {:quote/quote "Not very hospitable, are they?" :quote/source "Ensign Mendon"}
     {:quote/quote "That is not your concern. Observe your station, Ensign Mendon." :quote/source "Worf"}
     {:quote/quote "Didn't mean to offend you." :quote/source "Ensign Mendon"}
     {:quote/quote "You didn't. Yet." :quote/source "Worf"}
     {:quote/quote "Isn't that gagh?" :quote/source "William Riker"}
     {:quote/quote "Very good. You did some research on our nutritional choices." :quote/source "Lieutenant Klag"}
     {:quote/quote "Yes, but..." :quote/source "William Riker"}
     {:quote/quote "It's still moving." :quote/source "William Riker"}
     {:quote/quote "Gagh is always best when served live." :quote/source "Lieutenant Klag"}
     {:quote/quote "Would you like something easier?" :quote/source "Lieutenant Klag"}
     {:quote/quote "Easier?" :quote/source "William Riker"}
     {:quote/quote "Yes. If Klingon food is too strong for you, perhaps we could get one of the females to breast-feed you." :quote/source "Lieutenant Klag"}
     {:quote/quote "If we were not surrounded by all these people, you know what I would like to do?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Bust a chair across my teeth, probably." :quote/source "Phillipa Louvois"}
     {:quote/quote "After that." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Oh, ain't love wonderful?" :quote/source "Phillipa Louvois"}
     {:quote/quote "[to Picard] I never thought I'd say this, but it's good to see you again. It brings a sense of order and stability to my universe to know that you're still a pompous ass...and a damn sexy man." :quote/source "Phillipa Louvois"}
     {:quote/quote "Consider that in the history of many worlds there have always been disposable creatures. They do the dirty work. They do the work that no one else wants to do, because it's too difficult and too hazardous. With an army of Datas, all disposable, you don't have to think about their welfare, or you don't think about how they feel. Whole generations of disposable people." :quote/source "Guinan"}
     {:quote/quote "You're talking about slavery." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I think that's a little harsh." :quote/source "Guinan"}
     {:quote/quote "I don't think that's a little harsh, I think that's the truth. That's the truth that we have obscured behind...a comfortable, easy euphemism. 'Property.' But that's not the issue at all, is it?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "[demonstrating Data is a machine by deactivating him in the courtroom] Pinocchio is broken; its strings have been cut." :quote/source "William T. Riker"}
     {:quote/quote "Now tell me, Commander, what is Data?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I...I don't understand." :quote/source "Bruce Maddox"}
     {:quote/quote "[shouting] What is he?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "A machine!" :quote/source "Bruce Maddox"}
     {:quote/quote "Is he? Are you sure?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Yes!" :quote/source "Bruce Maddox"}
     {:quote/quote "You see, he's already met two of your criteria for sentience, so what about the third? Consciousness, in even the smallest degree! What is he then? I don't know. [to Maddox] Do you? [to Riker] Do you? [to Louvois] Do you?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "well, there it sits! [point to Data] Waiting." :quote/source "Picard: Your honor, the courtroom is a crucible; in it, we burn away irrelevancies until we are left with a purer product: the truth, for all time. Now sooner or later, this man [Commander Maddox] – or others like him – will succeed in replicating Commander Data. The decision you reach here today will determine how we will regard this creation of our genius. It will reveal the kind of people we are; what he is destined to be. It will reach far beyond this courtroom and this one android. It could significantly redefine the boundaries of personal liberty and freedom: expanding them for some, savagely curtailing them for others. Are you prepared to condemn him [Commander Data] – and all who will come after him – to servitude and slavery? Your Honor, Starfleet was founded to seek out new life"}
     {:quote/quote "It sits there looking at me, and I don't know what it is. This case has dealt with metaphysics, with questions best left to saints and philosophers. I am neither competent nor qualified to answer those. I've got to make a ruling, to try to speak to the future. Is Data a machine? Yes. Is he the property of Starfleet? No. We have all been dancing around the basic issue. Does Data have a soul? I don't know that he has. I don't know that I have! But I have got to give him the freedom to explore that question himself. It is the ruling of this court that Lieutenant Commander Data has the freedom to choose." :quote/source "Phillipa Louvois"}
     {:quote/quote "I formally refuse to undergo your procedure." :quote/source "Data"}
     {:quote/quote "Sir? There is a celebration on the holodeck." :quote/source "Data"}
     {:quote/quote "[despondent] I have no right to be there." :quote/source "William Riker"}
     {:quote/quote "Because you failed in your task?" :quote/source "Data"}
     {:quote/quote "Oh, God, no, I was that close to winning, Data." :quote/source "William Riker"}
     {:quote/quote "[considers the statement] Yes, sir." :quote/source "Data"}
     {:quote/quote "I almost cost you your life!" :quote/source "William Riker"}
     {:quote/quote "That is true, sir. But Commander... Will. I have learned from your example." :quote/source "Data"}
     {:quote/quote "[perplexed] What could you possibly have learned from that ordeal?" :quote/source "William Riker"}
     {:quote/quote "That at times, one must deny one's nature, sacrifice one's own personal beliefs, to protect another. Is it not true that had you refused to prosecute, Captain Louvois would have ruled summarily against me?" :quote/source "Data"}
     {:quote/quote "Yes." :quote/source "William Riker"}
     {:quote/quote "That action injured you, and saved me. I will not forget it." :quote/source "Data"}
     {:quote/quote "[smiles] You're a wise man, my friend." :quote/source "William Riker"}
     {:quote/quote "Not yet, sir. But with your help, I am learning." :quote/source "Data"}
     {:quote/quote "[about Salia] Do not be deceived by her looks. The body is just a shell." :quote/source "Worf"}
     {:quote/quote "That is how the Klingon lures a mate." :quote/source "Worf"}
     {:quote/quote "Are you telling me to go yell at Salia?" :quote/source "Wesley"}
     {:quote/quote "No. Men do not roar. Women roar...[besotted] and they hurl heavy objects...and claw at you..." :quote/source "Worf"}
     {:quote/quote "What does the man do?" :quote/source "Wesley"}
     {:quote/quote "He reads love poetry. [regaining his composure.] He ducks a lot." :quote/source "Worf"}
     {:quote/quote "Fate. It protects fools, little children, and ships named 'Enterprise.'' :quote/source 'William T. Riker"}
     {:quote/quote "If it should become necessary to fight, can you arrange to find me some rocks to throw at them?" :quote/source "William T. Riker"}
     {:quote/quote "That was not manual override." :quote/source "Data"}
     {:quote/quote "Blue, amber, amber, red." :quote/source "Data"}
     {:quote/quote "That's the launch sequence? [Data gives a jerky nod.] How do I override the doors?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Blue, blue, blue" :quote/source "Data"}
     {:quote/quote "I hope that is not a stutter." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Captain, you will be killed." :quote/source "Worf"}
     {:quote/quote "I'll go through the gate." :quote/source "Jean-Luc Picard"}
     {:quote/quote "But where will you end up?" :quote/source "Worf"}
     {:quote/quote "Very shortly anywhere will be preferable to this room." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I cannot deactivate the auto-destruct, but at least I have the satisfaction that you will die with us." :quote/source "Sub commander Taris"}
     {:quote/quote "[beaming out] Not, I think, today, Commander." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Six.  Six is your number." :quote/source "Croupier"}
     {:quote/quote "Hell, my blind grandma can make a six, boy!  Come on; roll 'em!'' :quote/source 'Gambler 1"}
     {:quote/quote "I thought seven and eleven had value." :quote/source "William Riker"}
     {:quote/quote "Actually, six is a valid point.  Of course, now the objective is to hit a duplicate six before hitting six." :quote/source "Data"}
     {:quote/quote "But the probability of making a six is no greater than that of rolling a seven." :quote/source "William Riker"}
     {:quote/quote "There is a certain degree of random fortune involved. I believe that is why they call it 'gambling.'' :quote/source 'Data"}
     {:quote/quote "Yeah." :quote/source "Gambler 2"}
     {:quote/quote "So much for your new turkey!" :quote/source "Gambler 2"}
     {:quote/quote "Commander, these cubes are improperly balanced. I believe their final resting position would be a..." :quote/source "Data"}
     {:quote/quote "Can you repair them?" :quote/source "Riker"}
     {:quote/quote "I believe so. I will make another attempt." :quote/source "Data"}
     {:quote/quote "Baby needs a new pair of shoes." :quote/source "Data"}
     {:quote/quote "Flair is what marks the difference between artistry and mere competence." :quote/source "William Riker"}
     {:quote/quote "The game's not big enough unless it scares you a little." :quote/source "William Riker"}
     {:quote/quote "the Romulans, the Klingons... They're nothing compared to what's waiting. Picard, you are about to move into areas of the galaxy containing wonders more incredible than you can possibly imagine... and terrors to freeze your soul. I offer myself as a guide -- only to be rejected out of hand." :quote/source "Q: You judge yourselves against the pitiful adversaries you've encountered so far"}
     {:quote/quote "(to Worf) Micro-brain! Growl for me, let me know you still care!" :quote/source "Q"}
     {:quote/quote "Travel time to the nearest starbase?" :quote/source "William T. Riker"}
     {:quote/quote "[OC] At maximum warp, in two years, seven months, three days, eighteen hours we would reach Starbase 185." :quote/source "Data"}
     {:quote/quote "Why?" :quote/source "William T. Riker"}
     {:quote/quote "Why? Why, to give you a taste of your future, a preview of things to come. Con permiso, Capitan. The hall is rented, the orchestra engaged. It's now time to see if you can dance." :quote/source "Q"}
     {:quote/quote "Guinan, your people have been in this part of the galaxy." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Yes." :quote/source "Guinan (quietly)"}
     {:quote/quote "What can you tell us?" :quote/source "William T. Riker"}
     {:quote/quote "Only that if I were you, I'd start back now." :quote/source "Guinan"}
     {:quote/quote "My people encountered them a century ago. They destroyed our cities, scattered my people throughout the galaxy. They're called the Borg. Protect yourself, Captain, or they'll destroy you." :quote/source "Guinan"}
     {:quote/quote "We have analyzed your defensive capabilities as being unable to withstand us. If you defend yourselves, you will be punished." :quote/source "The Borg"}
     {:quote/quote "The Borg is the ultimate user. They're unlike any threat your Federation has ever faced. They're not interested in political conquest, wealth or power as you know it. They're simply interested in your ship. Its technology. They've identified it as something they can consume." :quote/source "Q"}
     {:quote/quote "You brought us here, you exposed us to it, and you cost us the lives of our ship-mates!" :quote/source "William T. Riker"}
     {:quote/quote "Oh, please..." :quote/source "Q"}
     {:quote/quote "(to Riker) Number One. (to Q) Eighteen of our people have died. Please, tell us that this is one of your illusions." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Oh, no. This is as real as your so-called life gets. [disappears]" :quote/source "Q"}
     {:quote/quote "If you can't take a little bloody nose, maybe you ought to go back home and crawl under your bed. It's not safe out here! It's wondrous, with treasures to satiate desires both subtle and gross...but it's not for the timid." :quote/source "Q"}
     {:quote/quote "Q set a series of events into motion, bringing contact with the Borg much sooner than it should have come. Now, perhaps when you're ready, it might be possible to establish a relationship with them. But for now, for right now, you're just raw material to them. Since they're aware of your existence..." :quote/source "Guinan"}
     {:quote/quote "...they will be coming." :quote/source "Picard (realising the implications)"}
     {:quote/quote "You can bet on it." :quote/source "Guinan"}
     {:quote/quote "We are Pakleds. Our ship is the Mondor. [indicates some engineers trying to fix the ship's warp drive] It is broken." :quote/source "Grebnedlog"}
     {:quote/quote "(telling Wesley about his early career) My mates and I were at the Bonestell Recreation Facility, which was something of a dead rough crossroads at the time, filled with an assorted bunch of galactic riffraff. When a trio of Nausicaans came in, they were clearly spoiling for a confrontation with a group of fresh-faced Starfleet officers such as ourselves. Well, everyone in the group had the good sense to give these Nausicaans a wide berth, to stand off. Everyone, that is, except me. I stood toe to toe with the worst of the three, and I told him what I thought of him, his pals, his planet, and I possibly made some passing reference to his questionable parentage. And the next thing I knew, all three of them were on me and I was fighting for my life. I was actually doing quite well for a while, too." :quote/source "Jean-Luc Picard"}
     {:quote/quote "You fought them? And won?" :quote/source "Wesley Crusher"}
     {:quote/quote "I had this one Nausicaan down in this somewhat devious joint-lock when, unbeknownst to me, one of his chums drew his weapon and impaled me through the back. Curious sensation, actually. Not much pain. Shock, certainly, at the sight of serrated metal sticking through my chest. A certain giddy warmth. In fact I do actually remember that I laughed out loud." :quote/source "Jean-Luc Picard"}
     {:quote/quote "We are strong!" :quote/source "Reginod"}
     {:quote/quote "You're now armed to the teeth." :quote/source "Geordi La Forge"}
     {:quote/quote "Teeth are for chewing." :quote/source "Grebnedlog"}
     {:quote/quote "(disgustedly) Are you drunk yet, or can you go an' talk to Dr. Pulaski about the children?" :quote/source "Brenna"}
     {:quote/quote "What about them?" :quote/source "Danilo"}
     {:quote/quote "She wants to send them to school with the ship's children." :quote/source "Brenna"}
     {:quote/quote "W - what do you think?" :quote/source "Danilo"}
     {:quote/quote "I think it's a good idea. So go handle it! (She looks pointedly at other Bringloidi hanging around nearby) And I'm sure there's somethin' you can be doin' with your time...(she turns to Worf) And as for YOU -!" :quote/source "Brenna"}
     {:quote/quote "What?!" :quote/source "Worf (irritated and incredulous)"}
     {:quote/quote "Why'd you have to go an' tell them this magic wall can give 'em more than just meat an' potatoes? Now we'll never get a lick o' work out of them!" :quote/source "Brenna"}
     {:quote/quote "(still irritated) Madam - have you ever considered a career in security?!" :quote/source "Worf"}
     {:quote/quote "If it's anythin' like babysittin', I'm an authority!" :quote/source "Brenna"}
     {:quote/quote "Isn't that just like a man! You make these grandiose decisions, but you never stop to consider the poor women!" :quote/source "Brenna"}
     {:quote/quote "Miss Odell, I -" :quote/source "Picard"}
     {:quote/quote "You men draw a mug and solve all the problems of the world while the beer goes down, but, when it comes to the practical matters, it always falls to the women to make your grand dreams come true!" :quote/source "Brenna"}
     {:quote/quote "Miss Odell, you were the one who wanted a new home." :quote/source "Picard"}
     {:quote/quote "But I don't know if I want to be Eve!" :quote/source "Brenna"}
     {:quote/quote "It's your choice. If you wish, you can stay on the Enterprise. We will drop you at a starbase, then you can go where you wish." :quote/source "Picard"}
     {:quote/quote "Leave my da?" :quote/source "Brenna"}
     {:quote/quote "If this is going to work, these people will need your strength...your guidance." :quote/source "Picard"}
     {:quote/quote "(under her breath, gazing up as if to heaven) Oh, damn...(she looks thoughtfully at the Prime Minister) What does he do again?" :quote/source "Brenna"}
     {:quote/quote "Prime Minister." :quote/source "Picard"}
     {:quote/quote "Hmmm. Sounds important." :quote/source "Brenna"}
     {:quote/quote "Oh, it is." :quote/source "Picard"}
     {:quote/quote "Sounds like he might have more than two coins to rub together. (impish look) Three husbands...?" :quote/source "Brenna"}
     {:quote/quote "Judging a being by its physical appearance is the last major human prejudice, Wesley." :quote/source "Data"}
     {:quote/quote "It seems Captain that you are the early favorite. (to become her mother's husband)" :quote/source "Troi"}
     {:quote/quote "Congratulations Sir!" :quote/source "Riker"}
     {:quote/quote "I'm not amused, Number One." :quote/source "Picard"}
     {:quote/quote "You are late." :quote/source "Worf"}
     {:quote/quote "Sorry, had to make myself beautiful." :quote/source "K'Ehleyr"}
     {:quote/quote "I fail to understand why." :quote/source "Worf"}
     {:quote/quote "Worf, we’re alone now. You don’t have to act like a Klingon glacier, I don’t bite… actually, that’s not true, I do bite." :quote/source "K’Ehleyr"}
     {:quote/quote "You’re upset." :quote/source "Troi"}
     {:quote/quote "Your finely honed Betazoid sense tell you that?" :quote/source "K’Ehleyr"}
     {:quote/quote "Well… that and the table." :quote/source "Troi"}
     {:quote/quote "Lieutenant, I order you to relax." :quote/source "Picard"}
     {:quote/quote "I AM RELAXED!" :quote/source "Worf"}
     {:quote/quote "How’d you like command?" :quote/source "Riker"}
     {:quote/quote "Comfortable chair." :quote/source "Worf"}
     {:quote/quote "(in response to Kolrami's criticism of Riker's 'jocularity') Will Riker is the finest officer with whom I have ever served. Don't confuse style with intent.' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "It is possible to commit no mistakes and still lose. That is not a weakness; that is life." :quote/source "Jean-Luc Picard"}
     {:quote/quote "It is a matter of perspective, Doctor. In the strictest sense, I did not win...(beat, to disappointed groans from the crew)...I busted him up. (they laugh and cheer)" :quote/source "Data"}
     {:quote/quote "I hope these are the right coordinates. Just kidding, doctor. I know how much you love the transporter." :quote/source "Chief O'Brien"}
     {:quote/quote "About as much as I love comical transporter chiefs." :quote/source "Dr. Pulaski"}
     {:quote/quote "For Commander Riker's sake, I hope my hypothesis is in error." :quote/source "Data"}
     {:quote/quote "Unfortunately Commander Data, your hypotheses rarely are." :quote/source "Picard"}
     {:quote/quote "If you drop a hammer on your foot, it's hardly useful to get mad at the hammer." :quote/source "Riker"}
     {:quote/quote "Deanna, facing death is the ultimate test of character. I don't want to die but if I have to do, I'd like to do it with a little pride." :quote/source "Riker"}
     {:quote/quote "Now the burden is yours." :quote/source "Dr. Stubbs"}
     {:quote/quote "Burden?" :quote/source "Wesley Crusher"}
     {:quote/quote "To fulfill your potential. You will never come up against a greater adversary than your own potential, my young friend." :quote/source "Dr. Stubbs"}
     {:quote/quote "It's just a science project." :quote/source "Wesley Crusher"}
     {:quote/quote "You know, a doctor friend once said the same thing to me. Frankenstein was his name." :quote/source "Guinan"}
     {:quote/quote "Wes, do you think you're gonna get a good grade?" :quote/source "Guinan"}
     {:quote/quote "[sighs] I always get an A. [leaves Ten-Forward]" :quote/source "Wesley Crusher"}
     {:quote/quote "So did Dr Frankenstein." :quote/source "Guinan"}
     {:quote/quote "This is hopeless. Fighting would be preferable." :quote/source "Worf"}
     {:quote/quote "Pursuant to paragraph one thousand, two hundred and ninety, I hereby formally request third party arbitration of our dispute." :quote/source "Jean-Luc Picard"}
     {:quote/quote "You have the right." :quote/source "Sheliak Captain"}
     {:quote/quote "Furthermore, pursuant to subsection D3, I name ... the Grizzelas to arbitrate." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Grizzelas?" :quote/source "Sheliak Captain"}
     {:quote/quote "(mouths) Grizzelas?" :quote/source "William Riker"}
     {:quote/quote "Unfortunately, they are currently in their hibernation cycle. However, they will awaken in six months, at which time we can get this matter settled. Now, do you want to wait... or give me my three weeks?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Absurd! We carry the membership! We can brook no delay!" :quote/source "Sheliak Captain"}
     {:quote/quote "Then I hereby declare this treaty in abeyance." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Wait! Negotiation is permiss...!" :quote/source "Sheliak Captain"}
     {:quote/quote "You enjoyed that." :quote/source "William Riker"}
     {:quote/quote "You're damned right." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Things are only impossible until they're not." :quote/source "Jean-Luc Picard"}
     {:quote/quote "We're not qualified to be your judges. We have no law to fit your crime." :quote/source "Jean-Luc Picard"}
     {:quote/quote "We leave behind a being of extraordinary power... and conscience. I am not certain if he should be praised or condemned... only that he should be left alone." :quote/source "Jean-Luc Picard"}
     {:quote/quote "That is my home?" :quote/source "Nuria"}
     {:quote/quote "Seen from far, far above." :quote/source "Picard"}
     {:quote/quote "Yet we do not fall. I never imagined I would see the clouds from the other side. Your powers are truly boundless." :quote/source "Nuria"}
     {:quote/quote "Nuria, your people live in huts. Was it always so?" :quote/source "Picard"}
     {:quote/quote "No. We have found remnants of tools in caves. Our ancestors must have lived there." :quote/source "Nuria"}
     {:quote/quote "So why do you now live in huts?" :quote/source "Picard"}
     {:quote/quote "Huts are better. Caves are dark and wet." :quote/source "Nuria (matter-of-factly)"}
     {:quote/quote "If huts are better, why did you once live in caves?" :quote/source "Picard"}
     {:quote/quote "The most reasonable explanation would be that at one time we didn't know how to make huts." :quote/source "Nuria"}
     {:quote/quote "Just as at one time you did not know how to weave cloth, how to make a bow." :quote/source "Picard"}
     {:quote/quote "That would be reasonable." :quote/source "Nuria"}
     {:quote/quote "Someone invented a hut. Someone invented a bow, who taught others, who taught their children, who built a stronger hut, built a better bow, who taught their children. Now, Nuria, suppose one of your cave dwelling ancestors could see you as you are today. What would she think?" :quote/source "Picard"}
     {:quote/quote "I don't know." :quote/source "Nuria"}
     {:quote/quote "Put yourself in her place. You see, she cannot kill a hornbuck at a great distance. You can. You have a power she lacks." :quote/source "Picard"}
     {:quote/quote "Only because I have a bow." :quote/source "Nuria"}
     {:quote/quote "She's never seen a bow. It doesn't exist in her world. To you, it's a simple tool. To her, it's magic." :quote/source "Picard"}
     {:quote/quote "I suppose she might think so." :quote/source "Nuria"}
     {:quote/quote "Now, how would she react to you?" :quote/source "Picard"}
     {:quote/quote "I think she would fear me." :quote/source "Nuria"}
     {:quote/quote "Just as you fear me." :quote/source "Picard"}
     {:quote/quote "I do not fear you any longer." :quote/source "Nuria"}
     {:quote/quote "Good. That's good. You see, my people once lived in caves. And then we learned to build huts and, in time, to build ships like this one." :quote/source "Picard"}
     {:quote/quote "Perhaps one day, my people will travel above the skies." :quote/source "Nuria"}
     {:quote/quote "Of that, I have absolutely no doubt." :quote/source "Picard (softly, but with utter conviction and admiration)"}
     {:quote/quote "Crusher to Picard. I think we're going to lose Warren." :quote/source "Crusher"}
     {:quote/quote "On my way." :quote/source "Picard [OC]"}
     {:quote/quote "I'm here, Mary." :quote/source "Barron"}
     {:quote/quote "Prepare two cc's of norep." :quote/source "Crusher"}
     {:quote/quote "I'm sorry." :quote/source "Crusher"}
     {:quote/quote "Picard, you could not save her?" :quote/source "Nuria"}
     {:quote/quote "No." :quote/source "Picard"}
     {:quote/quote "You do have limits. You are not masters of life and death." :quote/source "Nuria"}
     {:quote/quote "No, we are not. We can cure many diseases and we can repair injuries, we can even extend life. (bleakly) But for all our knowledge, all our advances, we are just as mortal as you are. We're just as powerless to prevent the inevitable." :quote/source "Picard"}
     {:quote/quote "You are a remarkable people, but you are not superior beings. My people must be made to understand that." :quote/source "Nuria"}
     {:quote/quote "Now, Mister LaForge." :quote/source "Picard"}
     {:quote/quote "Aye, sir." :quote/source "'LaForge [OC]"}
     {:quote/quote "What is that?" :quote/source "Nuria"}
     {:quote/quote "A place where we can watch your people." :quote/source "Picard"}
     {:quote/quote "But why?" :quote/source "Fento"}
     {:quote/quote "To study you. To understand your ways. Discontinue, Mister LaForge." :quote/source "Picard"}
     {:quote/quote "Picard, why should a people so advanced want to learn about us?" :quote/source "Nuria"}
     {:quote/quote "We were once as you are now. To study you is to understand ourselves." :quote/source "Picard"}
     {:quote/quote "But why did you have to hide yourself from us?" :quote/source "Fento"}
     {:quote/quote "Because their presence would affect us, just as it affected me." :quote/source "Liko"}
     {:quote/quote "It is our highest law that we shall not interfere with other cultures." :quote/source "Picard"}
     {:quote/quote "Then revealing yourselves was an accident." :quote/source "Oji"}
     {:quote/quote "Oh, yes, and now we must leave you." :quote/source "Picard"}
     {:quote/quote "Why? There's so much you can teach us." :quote/source "Oji"}
     {:quote/quote "But that, too, would be interference. You must progress in your own way." :quote/source "Picard"}
     {:quote/quote "So we will. You have taught us there is nothing beyond our reach." :quote/source "Nuria"}
     {:quote/quote "Not even the stars." :quote/source "Picard (with a smile in his voice)"}
     {:quote/quote "Pah'kee." :quote/source "Nuria"}
     {:quote/quote "I wish you good journeys, Picard. Remember my people." :quote/source "Nuria"}
     {:quote/quote "Always." :quote/source "Picard"}
     {:quote/quote "Maybe if we felt any human loss as keenly as we feel one of those close to us, human history would be far less bloody." :quote/source "William Riker"}
     {:quote/quote "It is exactly as they left it, Number One. In the bottle. [seeing his confusion]... The ship in the bottle. [further confusion] Good Lord, didn't anybody here build ships in bottles when they were boys?" :quote/source "Picard"}
     {:quote/quote "I did not play with toys." :quote/source "Worf"}
     {:quote/quote "I was never a boy." :quote/source "Data"}
     {:quote/quote "[brightly] I did, sir." :quote/source "O'Brien"}
     {:quote/quote "Thank you, Mister O'Brien. Proceed." :quote/source "Picard"}
     {:quote/quote "[defensively] I did! I really did! Ships in bottles? Great fun!" :quote/source "O'Brien"}
     {:quote/quote "Lieutenant, I understand your feelings about the Romulans, but this is not the time or the place." :quote/source "Dr. Beverly Crusher"}
     {:quote/quote "If you had seen them kill your parents, you would understand, Doctor. It is always the time and the place for those feelings." :quote/source "Worf"}
     {:quote/quote "This Romulan didn't murder your parents. And you are the only one who can save his life." :quote/source "Beverly Crusher"}
     {:quote/quote "Then he will die." :quote/source "Worf"}
     {:quote/quote "[referring to Geordi's blindness] How did this happen?" :quote/source "Bochra"}
     {:quote/quote "I was born that way." :quote/source "Geordi La Forge"}
     {:quote/quote "And your parents let you live?" :quote/source "Bochra"}
     {:quote/quote "What kind of question is that! Of course they let me live." :quote/source "Geordi La Forge"}
     {:quote/quote "No wonder your race is weak. You waste time and resources on defective children." :quote/source "Bochra"}
     {:quote/quote "I have no more wish to die than you do." :quote/source "Bochra"}
     {:quote/quote "Bochra, there are times when it is necessary to die for one's ideals... do you believe this is one of those times?" :quote/source "Geordi La Forge"}
     {:quote/quote "Let's go find that beacon." :quote/source "Geordi La Forge"}
     {:quote/quote "Do all humans give up so easily?" :quote/source "Bochra"}
     {:quote/quote "Bochra, we're lost. Unless you've got something that can smell neutrinos." :quote/source "Geordi La Forge"}
     {:quote/quote "We have the sensor device you are carrying." :quote/source "Bochra"}
     {:quote/quote "Tricorder? It's not designed to detect neutrinos." :quote/source "Geordi La Forge"}
     {:quote/quote "Your eye device does. Connect them." :quote/source "Bochra"}
     {:quote/quote "That's crazy. The devices aren't compatible, it would be impossible to get an accurate reading... wait a minute. Wait a minute. I wouldn't need to get an accurate reading. I'd just need it to point us in the right direction, a neutrino geiger counter...! [sighs] It's still impossible." :quote/source "Geordi La Forge"}
     {:quote/quote "Why?" :quote/source "Bochra"}
     {:quote/quote "Because I can't see! Adapting the devices is delicate work, you can't do it by hand." :quote/source "Geordi La Forge"}
     {:quote/quote "Then I will be your eyes." :quote/source "Bochra"}
     {:quote/quote "My name is DaiMon Goss and these are my counsels, Kol and Dr Aridor. We'll need chairs." :quote/source "DaiMon Goss"}
     {:quote/quote "I'm Captain Picard of the Enterprise. I am serving as host for these proceedings." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Good. Then see to it we get some chairs." :quote/source "DaiMon Goss"}
     {:quote/quote "Let me explain." :quote/source "Picard"}
     {:quote/quote "Fine, fine, just have your Klingon servant get us some chairs." :quote/source "DaiMon Goss"}
     {:quote/quote "I am in charge of security!" :quote/source "Worf"}
     {:quote/quote "Then who gets the chairs?" :quote/source "DaiMon Goss"}
     {:quote/quote "You know, if this doesn't work, the thought of spending the rest of my life in here is none too appealing." :quote/source "Geordi La Forge"}
     {:quote/quote "There is a bright side, Geordi. You will have me to talk to." :quote/source "Data"}
     {:quote/quote "Captain, I am detecting life readings from the planet surface, as well as several small areas of thermal radiation and carbon dioxide emissions, indicative of combustion." :quote/source "Data"}
     {:quote/quote "Campfires, Data." :quote/source "Wesley Crusher"}
     {:quote/quote "Is that not what I said?'' :quote/source 'Data"}
     {:quote/quote "Yuta, you're an excellent chef but you speak in riddles." :quote/source "Riker"}
     {:quote/quote "Fifty-three years...and she hasn't aged in a day." :quote/source "Riker"}
     {:quote/quote "Your ambushes would be more successful if you bathed more often!" :quote/source "Worf"}
     {:quote/quote "How do you allow Klingon peta'Q to walk around in a Starfleet uniform?" :quote/source "Jarok"}
     {:quote/quote "You are lucky this is not a Klingon ship. We know how to deal with spies." :quote/source "Worf"}
     {:quote/quote "Remove this tohzah from my sight!" :quote/source "Jarok"}
     {:quote/quote "Oh, what a fool I've been, to go looking for courage in the land of cowards!" :quote/source "Jarok"}
     {:quote/quote "I expected more than an idle threat from you, Picard." :quote/source "Commander Tomalak"}
     {:quote/quote "Then you shall have it!  Mr. Worf?" :quote/source "Captain Jean-Luc Picard"}
     {:quote/quote "Aye, sir!" :quote/source "Worf"}
     {:quote/quote "Klingon warships, armed and ready, sir!" :quote/source "Worf"}
     {:quote/quote "What shall it be, Tomalak?" :quote/source "Captain Jean-Luc Picard"}
     {:quote/quote "You will still not survive our assault." :quote/source "Commander Tomalak"}
     {:quote/quote "And you will not survive ours. Shall we die together?" :quote/source "Captain Jean-Luc Picard"}
     {:quote/quote "I did it for nothing!" :quote/source "Jarok"}
     {:quote/quote "Do not relax your security for an instant, Captain. He is extremely violent, and very cunning, as you already know." :quote/source "Prime Minister Nayrok"}
     {:quote/quote "the age-old cry of the oppressor." :quote/source "Jean-Luc Picard: A matter of internal security"}
     {:quote/quote "Danar! You are cunning... you must have Klingon blood." :quote/source "Worf"}
     {:quote/quote "either try to force them back or welcome them home. In your own words, this is not our affair. We cannot interfere with the natural course of your society's development and I'd say it's likely to develop significantly during the next several minutes." :quote/source "Jean-Luc Picard: We have everything we need for our report. Your prisoner has been returned to you. You have a decision to make"}
     {:quote/quote "Success, Captain?" :quote/source "William T. Riker"}
     {:quote/quote "Number One, note in your report that if the government of Angosia survives the night, we will offer Federation assistance in the efforts to reprogram their veterans." :quote/source "Jean-Luc Picard"}
     {:quote/quote "And if the government doesn't survive?" :quote/source "William T. Riker"}
     {:quote/quote "I have a feeling they will choose to." :quote/source "Jean-Luc Picard"}
     {:quote/quote "In a world where children blow up children, everyone's a threat." :quote/source "Alexana Devos"}
     {:quote/quote "You added the chair, Captain, I am just making you sit in it." :quote/source "Kyril Finn"}
     {:quote/quote "He's prepared to kill you." :quote/source "Dr. Beverly Crusher"}
     {:quote/quote "An excellent reason to escape." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I have been reviewing the history of armed rebellion and it appears that terrorism is an effective way to promote political change." :quote/source "Data"}
     {:quote/quote "Yes, it can be, but I have never subscribed to the theory that political power flows from the barrel of a gun." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Q!" :quote/source "Picard"}
     {:quote/quote "[playfully mocking his embarrassment] Red alert!" :quote/source "Q"}
     {:quote/quote "And since I was given only a fraction of a second to mull, I chose this, and asked them to bring me here." :quote/source "Q"}
     {:quote/quote "Why?" :quote/source "Deanna Troi"}
     {:quote/quote "Because in all the universe, you are the closest thing I have to a friend, Jean-Luc." :quote/source "Q"}
     {:quote/quote "I have no powers! Q the ordinary!" :quote/source "Q"}
     {:quote/quote "Q the liar! Q the misanthrope!" :quote/source "Picard"}
     {:quote/quote "Q the miserable, Q the desperate! What must I do to convince you people?" :quote/source "Q"}
     {:quote/quote "Die!" :quote/source "Worf"}
     {:quote/quote "Oh, very clever, Worf. Eat any good books lately?" :quote/source "Q"}
     {:quote/quote "(to Worf) I can't disappear any more than you could win a beauty contest." :quote/source "Q"}
     {:quote/quote "If I ask a very simple question, do you think you might be able to answer it without it troubling your intellect too much? (He enters the cell) Ready? Here goes. Would I permit you to lock me away if I still had all my powers?" :quote/source "Q"}
     {:quote/quote "You have fooled us too often, Q." :quote/source "Worf"}
     {:quote/quote "Perspicacity incarnate! Please don't feel compelled now to tell me the story of the boy who cried Worf." :quote/source "Q"}
     {:quote/quote "Can I have a Starfleet uniform? What are you looking at?" :quote/source "Q"}
     {:quote/quote "I was considering the possibility that you are telling the truth, that you really are human." :quote/source "Data"}
     {:quote/quote "It's the ghastly truth, Mr. Data. I can now stub my toe with the best of them." :quote/source "Q"}
     {:quote/quote "An irony. It means that you have achieved in disgrace what I have always aspired to be." :quote/source "Data"}
     {:quote/quote "It's easy. Just change the gravitational constant of the universe." :quote/source "Q"}
     {:quote/quote "I'm not good in groups. It's hard to work in groups when you're omnipotent." :quote/source "Q"}
     {:quote/quote "Dr Crusher, Starfleet have sent you into exile again." :quote/source "Q"}
     {:quote/quote "Your bedside manner is admirable, Doctor. I'm sure your patients must recover quickly... (sotto voce) just to get away from you." :quote/source "Q"}
     {:quote/quote "I'll have 10 chocolate sundaes." :quote/source "Q"}
     {:quote/quote "I have never seen anyone eat 10 chocolate sundaes." :quote/source "Data"}
     {:quote/quote "I'm in a really bad mood. And since I've never eaten before, I should be very hungry." :quote/source "Q"}
     {:quote/quote "This is not a moment I've been looking forward to." :quote/source "Q (with foreboding as Guinan enters Ten-Forward)"}
     {:quote/quote "I hear they drummed you out of the Continuum." :quote/source "Guinan"}
     {:quote/quote "I like to think of it as a significant career change." :quote/source "Q"}
     {:quote/quote "Just one of the boys, eh?" :quote/source "Guinan (sardonic)"}
     {:quote/quote "One of the boys with an IQ of 2005." :quote/source "Q"}
     {:quote/quote "The Captain and many of the crew are not yet convinced he is truly human." :quote/source "Data"}
     {:quote/quote "Really?" :quote/source "Guinan"}
     {:quote/quote "AAAAAHHHH!!!" :quote/source "Q"}
     {:quote/quote "Seems human enough to me." :quote/source "Guinan (coolly)"}
     {:quote/quote "The Calamarain are not very hospitable creatures. They exist as swirls of ionised gas." :quote/source "Q"}
     {:quote/quote "What did you do to them, Q?" :quote/source "Picard"}
     {:quote/quote "Nothing bizarre, nothing grotesque." :quote/source "Q"}
     {:quote/quote "You tormented them." :quote/source "Riker"}
     {:quote/quote "A subjective term, Riker. One creature's torment, is another creature's delight. They simply have no sense of humour, a character flaw with which you can personally identify." :quote/source "Q"}
     {:quote/quote "I say we turn him over to them." :quote/source "Riker"}
     {:quote/quote "Oh, I take it back. You do have a sense of humour. A dreadful one at that." :quote/source "Q"}
     {:quote/quote "I'm serious." :quote/source "Riker"}
     {:quote/quote "I wasn't the one who misplaced the entire Deltivid Asteroid Belt!" :quote/source "Q"}
     {:quote/quote "This is getting on my nerves, now that I have them!" :quote/source "Q"}
     {:quote/quote "All right, everyone, this is what we're going to be doing." :quote/source "Q"}
     {:quote/quote "Q, everybody already knows what they're going to do, except for you, Now here's what I need." :quote/source "LaForge"}
     {:quote/quote "La Forge, obviously my knowledge and experience far exceed yours by about a billion times. So, if you'll just step aside gracefully." :quote/source "Q"}
     {:quote/quote "Q, your experience will be most valuable to me if you can manually control the field integrity." :quote/source "LaForge"}
     {:quote/quote "Don't be foolish. That would be a waste of my talents." :quote/source "Q"}
     {:quote/quote "Q, get to the controls or get the hell out of here! Data, you're my liaison to the Bridge. I'll need you with me." :quote/source "LaForge"}
     {:quote/quote "Who does he think he is, giving me orders?" :quote/source "Q"}
     {:quote/quote "Geordi thinks he is in command here...and he is correct." :quote/source "Data"}
     {:quote/quote "Q, you exceed your own standards for self-preoccupation." :quote/source "Jean-Luc Picard"}
     {:quote/quote "[After Q stole a shuttle] This goes against my better judgment. Transporter Room 3, lock onto Shuttle 1 and beam it back into its bay." :quote/source "Picard"}
     {:quote/quote "Aye, sir." :quote/source "Crewman"}
     {:quote/quote "[after Riker gives him a disapproving look] It's a perfectly good shuttlecraft!" :quote/source "Picard"}
     {:quote/quote "[After Q gives him 2 attractive women] I don't need your fantasy women!" :quote/source "Riker"}
     {:quote/quote "Oh, you're so stolid! You weren't like that before the beard." :quote/source "Q"}
     {:quote/quote "All right, try this." :quote/source "Guinan"}
     {:quote/quote "What is it?" :quote/source "Worf"}
     {:quote/quote "Just try it." :quote/source "Guinan"}
     {:quote/quote "You see? It's an Earth drink. Prune juice." :quote/source "Guinan"}
     {:quote/quote "[awestruck] A warrior's drink." :quote/source "Worf"}
     {:quote/quote "How can I ask them to sacrifice themselves based solely on your intuition?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I don't know. But I do know that this is a mistake. Every fiber in my being says this is a mistake. I can't explain it to myself, so I can't explain it to you. I only know that I'm right." :quote/source "Guinan"}
     {:quote/quote "Who is to say that this history is any less proper than the other?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I suppose I am." :quote/source "Guinan"}
     {:quote/quote "Not good enough, damn it, not good enough! I will not ask them to die!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "40 billion people have already died. This war is not supposed to be happening. You've got to send those people back to correct this." :quote/source "Guinan"}
     {:quote/quote "And what is to guarantee that if they go back, they will succeed? Every instinct is telling me this is wrong, it is dangerous, it is futile!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "We've known each other a long time. You have never known me to impose myself on anyone, or take a stance based on trivial or whimsical perceptions. This timeline must not be allowed to continue. Now, I've told you what you must do. You have only your trust in me to help you decide to do it." :quote/source "Guinan"}
     {:quote/quote "To be honest with you, Picard... a significant number of my crew members have expressed the desire to return, even knowing the odds. Some because they can't bear to live without their loved ones; some because they don't like the idea of slipping out in the middle of a fight. But I have told them that, in the here and now the Federation needs another ship against the Klingons, and we'd better get used to being in the here and now." :quote/source "Rachel Garrett"}
     {:quote/quote "But, if you go back, it could be a great deal more helpful. The war is going very badly for the Federation... far worse than is generally known. Starfleet Command believes that defeat is inevitable. Within six months, we may have no choice but to surrender." :quote/source "Jean-Luc Picard"}
     {:quote/quote "And you're saying that all this may be a result of our arrival here?" :quote/source "Rachel Garrett"}
     {:quote/quote "One more ship will make no difference in the here and now, but 22 years ago, one ship could have stopped this war before it started." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Mr. Castillo." :quote/source "Rachel Garrett"}
     {:quote/quote "Yes, Captain." :quote/source "Richard Castillo"}
     {:quote/quote "Inform the crew we're going back." :quote/source "Rachel Garrett"}
     {:quote/quote "Yes, Captain." :quote/source "Richard Castillo"}
     {:quote/quote "The Romulans will get a good fight. We'll make it one for the history books." :quote/source "Rachel Garrett"}
     {:quote/quote "Can I get you something, Tasha?" :quote/source "Guinan"}
     {:quote/quote "Guinan, I have to know something. What happens to me in the other time line?" :quote/source "Tasha Yar"}
     {:quote/quote "I don't have alternate biographies of the crew. As I said to the captain, it's just a feeling." :quote/source "Guinan"}
     {:quote/quote "But there's something more when you look at me, isn't there? I can see it in your eyes, Guinan. We've known each other too long." :quote/source "Tasha Yar"}
     {:quote/quote "We weren't meant to know each other at all! At least...that's what I sense when I look at you. Tasha, you're not supposed to be here." :quote/source "Guinan"}
     {:quote/quote "Where am I supposed to be?" :quote/source "Tasha Yar"}
     {:quote/quote "Dead." :quote/source "Guinan"}
     {:quote/quote "Do you know how?" :quote/source "Tasha Yar"}
     {:quote/quote "No. But I do know it was an empty death. A death without purpose." :quote/source "Guinan"}
     {:quote/quote "Come. Yes, Lieutenant?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Captain, I request a transfer to the Enterprise-C." :quote/source "Tasha Yar"}
     {:quote/quote "For what reason?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "They need someone at Tactical." :quote/source "Tasha Yar"}
     {:quote/quote "We need you here." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I'm not supposed to be here, sir." :quote/source "Tasha Yar"}
     {:quote/quote "Sit down, Lieutenant. What did she say to you?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I don't belong here, sir. I'm supposed to be...dead." :quote/source "Tasha Yar"}
     {:quote/quote "She felt it necessary to reveal that to you?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I felt it was necessary." :quote/source "Tasha Yar"}
     {:quote/quote "I see. You realise that it is very possible the Enterprise-C will fail. We will continue in this time line, in which case your life, hopefully, will continue for a long while." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I know how important it is that they don't fail, Captain. That's why I'm requesting this transfer." :quote/source "Tasha Yar"}
     {:quote/quote "You don't belong on that ship, Lieutenant." :quote/source "Jean-Luc Picard"}
     {:quote/quote "No, Captain Garrett belongs on that ship. But she's dead. And I think there's a certain logic in this request." :quote/source "Tasha Yar"}
     {:quote/quote "There's no logic in this at all! Whether they succeed or not, the Enterprise-C will be destroyed!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "But Captain, at least with someone at Tactical they will have a chance to defend themselves well. It may be a matter of seconds or minutes, but those could be the minutes that change history. Guinan says I died a senseless death in the other time line. I didn't like the sound of that, Captain. I've always known the risks that come with a Starfleet uniform. If I'm to die in one, I'd like my death to count for something." :quote/source "Tasha Yar"}
     {:quote/quote "Lieutenant...permission granted." :quote/source "Jean-Luc Picard (after a thoughtful pause)"}
     {:quote/quote "Thank you, sir." :quote/source "Tasha Yar"}
     {:quote/quote "(prior to battle) Attention all hands! As you know, we could outrun the Klingon vessels, but we must protect the Enterprise-C until she re-enters the temporal rift. And we must succeed. Let's make sure that history never forgets the name Enterprise." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Federation Ship Enterprise. Surrender and prepare to be boarded." :quote/source "Klingon"}
     {:quote/quote "[grimly] That'll be the day." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Geordi, tell me about Tasha." :quote/source "Guinan"}
     {:quote/quote "Doctor? I require your advice as a successful parent." :quote/source "Data"}
     {:quote/quote "Well, thank you, Data. I'd like to think I was. Well, please sit down. How's Lal?" :quote/source "Crusher"}
     {:quote/quote "Lal is realising she is not the same as other children." :quote/source "Data"}
     {:quote/quote "Is it lonely for her?" :quote/source "Crusher"}
     {:quote/quote "She does not feel the emotion of loneliness, but she can observe how isolated she is from the others. She wishes to be more like them. I do not know how to help her. Lal is passing into sentience. It is perhaps the most difficult stage of her development." :quote/source "Data"}
     {:quote/quote "When Wesley was growing up, he was an extraordinarily bright boy, but he had a hard time making friends. I think the other children were a little intimidated by him." :quote/source "Crusher"}
     {:quote/quote "That is precisely what happened to Lal in school. How did you help him?" :quote/source "Data"}
     {:quote/quote "Well, first I went back to my own childhood and remembered how painful it was for me. Because I remember a time when I wasn't very popular either. And when I told that to Wesley, it made him feel a little better. He knew I understood what he was going through." :quote/source "Crusher"}
     {:quote/quote "I have not told Lal how difficult it was for me to assimilate. I did not wish it to discourage her. Perhaps this was an error of judgement." :quote/source "Data"}
     {:quote/quote "You didn't have anyone experienced to help you through sentience. She at least has you. Just help her realise that she's not alone, and be there to nurture her when she needs love and attention." :quote/source "Crusher"}
     {:quote/quote "I can give her attention, Doctor. But I am incapable of giving her love. (Data leaves)" :quote/source "Data"}
     {:quote/quote "Now why do I find that so hard to believe?" :quote/source "Crusher  (softly thoughtful)"}
     {:quote/quote "Then he is questioning my ability as a parent." :quote/source "Data"}
     {:quote/quote "In a manner of speaking." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Does the Admiral have children, sir?" :quote/source "Data"}
     {:quote/quote "Yes, I believe he does, Data. Why?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I am forced to wonder how much experience he had as a parent when his first child was born." :quote/source "Data"}
     {:quote/quote "You see?" :quote/source "Guinan"}
     {:quote/quote "What are they doing?" :quote/source "Lal"}
     {:quote/quote "It's called flirting." :quote/source "Guinan"}
     {:quote/quote "They seem to be communicating telepathically." :quote/source "Lal"}
     {:quote/quote "They're both thinking the same thing, if that's what you mean." :quote/source "Guinan (wryly amused)"}
     {:quote/quote "I watch them and I can do the things they do but I will never feel the emotions. I'll never know love." :quote/source "Lal"}
     {:quote/quote "It is a limitation we must learn to accept, Lal." :quote/source "Data"}
     {:quote/quote "Then why do you still try to emulate humans? What purpose does it serve except to remind you that you are incomplete?" :quote/source "Lal"}
     {:quote/quote "I have asked myself that many times as I have struggled to be more human - until I realised it is the struggle itself that is most important. We must strive to be more than we are, Lal. It does not matter that we will never reach our ultimate goal. The effort yields its own rewards." :quote/source "Data"}
     {:quote/quote "You are wise, Father." :quote/source "Lal"}
     {:quote/quote "It is the difference between knowledge and experience." :quote/source "Data"}
     {:quote/quote "I learned today that humans like to hold hands. It is a symbolic gesture of affection." :quote/source "Lal"}
     {:quote/quote "Lal is my child. You ask that I volunteer to give her up. I cannot. It would violate every lesson I have learned about human parenting. I have brought a new life into this world, and it is my duty, not Starfleet's, to guide her through these difficult steps to maturity. To support her as she learns. To prepare her to be a contributing member of society. No one can relieve me from that obligation. And I cannot ignore it. I am...her father." :quote/source "Data"}
     {:quote/quote "There are times, sir, when men of good conscience cannot blindly follow orders. You acknowledge their sentience, but ignore their personal liberties and freedom. Order a man to turn his child over to the state? Not while I'm his captain." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Come in." :quote/source "Troi"}
     {:quote/quote "Hello, Lal. How are you?" :quote/source "Troi"}
     {:quote/quote "Troi. Admiral. Admiral. An admiral from Starfleet has come to take me away, Troi. I am scared." :quote/source "Lal"}
     {:quote/quote "You are scared, aren't you?" :quote/source "Troi (stunned as she realises she can actually feel Lal's fear)"}
     {:quote/quote "I feel it. How is this possible?" :quote/source "Lal"}
     {:quote/quote "I don't know." :quote/source "Troi"}
     {:quote/quote "This is what it means to feel. This is what it means to feel." :quote/source "Lal"}
     {:quote/quote "His hands were moving faster than I could see, trying to stay ahead of each breakdown. He refused to give up. He was remarkable...it just - wasn't meant to be." :quote/source "Admiral Haftel"}
     {:quote/quote "I love you, father." :quote/source "Lal"}
     {:quote/quote "I wish I could feel it with you." :quote/source "Data"}
     {:quote/quote "Then I will feel it for both of us." :quote/source "Lal"}
     {:quote/quote "Thank you for my life." :quote/source "Lal"}
     {:quote/quote "(high-ranking Klingon) This is not the Federation, Picard. If you defy an order of the High Council, the alliance with the Federation could fall to dust." :quote/source "K'mpek"}
     {:quote/quote "The alliance with the Federation is not based on lies, K'mpec. Protect your secrets if you must, but you will not sacrifice these men." :quote/source "Jean-Luc Picard"}
     {:quote/quote "This is not your world, human. You do not command here." :quote/source "Duras (Klingon)"}
     {:quote/quote "I'm not here to command." :quote/source "Jean-Luc Picard"}
     {:quote/quote " Then you must be ready to fight. Something Starfleet does not teach you." :quote/source "Duras"}
     {:quote/quote "You may test that assumption at your convenience." :quote/source "Jean-Luc Picard"}
     {:quote/quote "It's edible, but I wouldn't call it food." :quote/source "Kova Tholl"}
     {:quote/quote "You're destroying yourself and anyone who is foolish enough to listen to you!" :quote/source "Picard's Look-Alike"}
     {:quote/quote "You've shown none of the concern that Captain Picard would for the safety of his ship, the welfare of his crew." :quote/source "Riker"}
     {:quote/quote "Is the entire crew aware of this little scheme to send me off on holiday?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I believe there are two ensigns stationed on deck 39 who know nothing about it." :quote/source "William Riker"}
     {:quote/quote "Will you at least try to stay out of trouble?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I always try." :quote/source "Vash"}
     {:quote/quote '"And is that the purpose of existence? To care for someone?'' :quote/source 'Data"}
     {:quote/quote '"Through their joining they have been healed. Grief was transmuted to joy, loneliness to belonging.'' :quote/source 'Data"}
     {:quote/quote "I mean I am the guy who writes down things to remember to say when there is a party. And then when he finally gets there he winds up alone, in the corner, trying to look... comfortable examining a potted plant." :quote/source "Reginald Barclay"}
     {:quote/quote "You're just shy." :quote/source "Geordi La Forge"}
     {:quote/quote "Sounds as if it's nothing serious, doesn't it? You can't know." :quote/source "Reginald Barclay"}
     {:quote/quote "Delete all programs filed under 'Reginald Barclay'...(the computer gives audio acknowledgement, and then he grins slyly)...except program 9." :quote/source "Reginald Barclay"}
     {:quote/quote "For an android with no feelings, he sure managed to evoke them in others." :quote/source "William Riker"}
     {:quote/quote "That's all you have to do, go ahead. Fire. If only you could feel rage over Varria's death. If only you could feel a need for revenge, then maybe you could fire. But you're just an android. You can't feel anything, can you? It's just another interesting intellectual puzzle for you, another of life's curiosities." :quote/source "Fajo"}
     {:quote/quote "[to himself] I cannot permit this to continue." :quote/source "Data"}
     {:quote/quote "[handing his weapon to Riker] A Varon-T disruptor. It belongs to Fajo." :quote/source "Data"}
     {:quote/quote "Mr O'Brien says the weapon was in a state of discharge." :quote/source "Riker"}
     {:quote/quote "Perhaps something occurred during transport, Commander." :quote/source "Data"}
     {:quote/quote "You have lost everything you value." :quote/source "Data"}
     {:quote/quote "It must give you great pleasure." :quote/source "Fajo"}
     {:quote/quote "No, sir, it does not. I do not feel pleasure. I am only an android." :quote/source "Data"}
     {:quote/quote "Is it my imagination, or have tempers become a little frayed on the ship lately?" :quote/source "William Riker"}
     {:quote/quote "I hadn't noticed." :quote/source "Worf"}
     {:quote/quote "I see what you mean." :quote/source "Worf"}
     {:quote/quote "NOOOOOOOOOOOOOOOOOOO!!!! It is... it is... wrong! IT IS WRONG!! A lifetime of discipline washed away, and in its place... [laughs awkwardly, then grunts] Bedlam. BEDLAM! I'm so old. There is nothing left but dry bones, and dead friends. Oh... tired. Oh, so tired." :quote/source "Jean-Luc Picard"}
     {:quote/quote "It will pass, all of it. Just another hour or so, you're doing fine, just hold on." :quote/source "Beverly Crusher"}
     {:quote/quote "NO! This weakness disgusts me! I HATE IT! Where is my logic? I'm betrayed by... desires. Oh, I want to feel, I want to feel... everything. But I am... a Vulcan. I must feel nothing! Give me back my control! [he sobs uncontrollably]" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Jean-Luc!" :quote/source "Beverly Crusher"}
     {:quote/quote "Pe- Perrin... Amanda... I - wanted - to give you - so much more. I wanted to show you such... t- t- tenderness. But that is not our way. Spock... Amanda... Did you know? P-Perrin, can you - know... how - much I - love - you? I - do - LOVE YOU!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Beverly..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I'm here, Jean-Luc. I'm not going anywhere." :quote/source "Beverly Crusher"}
     {:quote/quote "It's... quite difficult. The anguish of the man. The despair... pouring out of him, all those feelings. The regrets. [sobs] I c- I can't... stop them! I can't stop them, I can't. I can't...! [breaks down crying]" :quote/source "Jean-Luc Picard"}
     {:quote/quote "[comforting him] Don't even try." :quote/source "Beverly Crusher"}
     {:quote/quote "I will take my leave of you now, Captain. I do not think we shall meet again." :quote/source "Ambassador Sarek"}
     {:quote/quote "I hope you are wrong, Ambassador." :quote/source "Jean-Luc Picard"}
     {:quote/quote "[referring to their mind-meld] We shall always retain the best part of the other... inside us." :quote/source "Sarek"}
     {:quote/quote "I believe I have the better part of that bargain, Ambassador. [raises his hand in the Vulcan salute] Peace and long life." :quote/source "Jean-Luc Picard"}
     {:quote/quote "[returns the salute] Live long and prosper." :quote/source "Sarek"}
     {:quote/quote "The Academy must make you wait- that's true. But upon reviewing your service to this ship, your crewmates, I cannot in good conscience make you wait for the Academy. You see, Wesley, in my eyes you are an acting Ensign in title only. I hereby grant you Field Promotion to Full Ensign with all the commensurate responsibilities and privileges of that rank. Congratulations. You're dismissed." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Less talk! More synthehol!" :quote/source "Worf"}
     {:quote/quote "They tried to convince us it was a sickness we would never survive, that the pain and energy pulses would kill us. They claimed we were dangerous, so they destroyed anyone who exhibited the signs of the transfiguration." :quote/source "John"}
     {:quote/quote "We were protecting our society!" :quote/source "Sunad"}
     {:quote/quote "By murdering us? You saw the mutations as a threat to your authority, you were terrified of something you couldn't understand. Some suspected that what was happening to them was not evil. Four of us decided to flee Zalkon and let the metamorphosis take its course. You hunted us down, killed the others. But I survived, with the help of a kind and generous people." :quote/source "John"}
     {:quote/quote "My people are about to embark on a new realm, a new plane of existence, thanks to you." :quote/source "John"}
     {:quote/quote "It is our mission to seek out life, in all forms. We are privileged to have been present at the emergence of a new species." :quote/source "Picard"}
     {:quote/quote "Early bird gets the worm? I believe Commander Shelby erred. There is no evidence of avian or crawling vermicular lifeforms on Jouret IV." :quote/source "Data"}
     {:quote/quote "That's not what she meant, Data. But you are right. She erred." :quote/source "Geordi"}
     {:quote/quote "It's something of a tradition, Guinan - Captain touring the ship before a battle." :quote/source "Picard"}
     {:quote/quote "Hmm. Before a hopeless battle, if I remember the tradition correctly." :quote/source "Guinan"}
     {:quote/quote "Not necessarily. Nelson toured the HMS Victory before Trafalgar." :quote/source "Picard"}
     {:quote/quote "Yes, but Nelson never returned from Trafalgar, did he?" :quote/source "Guinan"}
     {:quote/quote "No, but the battle was won." :quote/source "Picard"}
     {:quote/quote "I wonder if the Emperor Honorius watching the Visigoths coming over the seventh hill could truly realize that the Roman Empire was about to fall. This is really just another page of history, isn't it? Will this be the end of our civilization? Turn the page." :quote/source "Picard"}
     {:quote/quote "(gently) This isn't the end." :quote/source "Guinan"}
     {:quote/quote "You say that with remarkable assuredness." :quote/source "Picard"}
     {:quote/quote "With experience. When the Borg destroyed my world, my people were scattered throughout the Universe. We survived. As will humanity survive. As long as there's a handful of you to keep the spirit alive, you will prevail...even if it takes a millennium." :quote/source "Guinan"}
     {:quote/quote "Captain Jean-Luc Picard. You lead the strongest ship of the Federation Starfleet. You speak for your people." :quote/source "The Borg"}
     {:quote/quote "I have nothing to say to you. And I will resist you with my last ounce of strength!" :quote/source "Picard"}
     {:quote/quote "Strength is irrelevant. Resistance is futile. We wish to improve ourselves. We will add your biological and technological distinctiveness to our own. Your culture will adapt to service ours." :quote/source "The Borg"}
     {:quote/quote "Impossible! My culture is based on freedom and self-determination!" :quote/source "Picard"}
     {:quote/quote "Freedom is irrelevant. Self-determination is irrelevant. You must comply." :quote/source "The Borg"}
     {:quote/quote "We would rather die." :quote/source "Picard"}
     {:quote/quote "Death is irrelevant. Your archaic cultures are authority driven. To facilitate our introduction into your societies, it has been decided that a human voice will speak for us in all communications. You have been chosen to be that voice." :quote/source "The Borg"}
     {:quote/quote "I am Locutus of Borg. Resistance is futile. Your life as it has been… is over. From this time forward, you will service… us." :quote/source "Locutus"}
     {:quote/quote "Mr. Worf... fire." :quote/source "Riker"}
     {:quote/quote "They couldn't have adapted that quickly!" :quote/source "William Riker"}
     {:quote/quote "[to Riker] The knowledge and experience of the human Picard is part of us now. It has prepared us for all possible courses of action. Your resistance is hopeless... Number One." :quote/source "Locutus"}
     {:quote/quote "Now, how the hell do we defeat an enemy that knows us better than we know ourselves?" :quote/source "Riker"}
     {:quote/quote "The Borg have neither honour nor courage. That is our greatest advantage." :quote/source "Worf"}
     {:quote/quote "We will proceed to Earth.  And if you attempt to intervene, we will destroy you." :quote/source "Locutus"}
     {:quote/quote "Then take your best shot, Locutus. 'Cause we're about to 'intervene.'' :quote/source 'Riker"}
     {:quote/quote "They're ignoring the saucer section completely!" :quote/source "Wesley"}
     {:quote/quote "Just as you should, Captain.  Ensign, evasive pattern, Riker Beta." :quote/source "Riker"}
     {:quote/quote "Riker Beta confirmed." :quote/source "Wesley"}
     {:quote/quote "Proceed to second phase, Commander Shelby." :quote/source "Riker"}
     {:quote/quote "Acknowledged.  Firing anti-matter spread." :quote/source "Shelby"}
     {:quote/quote "Data, cut your engines!  Take her in unpowered." :quote/source "Riker"}
     {:quote/quote "Sleep, Data." :quote/source "Picard"}
     {:quote/quote "(with sympathy) He's exhausted." :quote/source "Dr. Crusher"}
     {:quote/quote "(thoughtfully) Yes, Doctor. But if I may make a supposition...I do not believe his message was intended to express fatigue - but to suggest a course of action...!" :quote/source "Data"}
     {:quote/quote "DATA!" :quote/source "Worf"}
     {:quote/quote "Mission accomplished; we have him!" :quote/source "Worf"}
     {:quote/quote "Firing shuttle thrusters." :quote/source "Data"}
     {:quote/quote "How do you feel?" :quote/source "Troi"}
     {:quote/quote "Almost human...with just a bit of a headache." :quote/source "Picard"}
     {:quote/quote "You know, you don't seem so arro... arro... Y'know!" :quote/source "Rene Picard"}
     {:quote/quote "Arrogant?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Yes! Arrogant! You don't seem that way to me. What does it mean, anyway? Arrogant Son of a-" :quote/source "Rene Picard"}
     {:quote/quote "Let's... Talk about that later, shall we?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "They took everything I was. They used me to kill and to destroy and I couldn't stop them. I should have been able to stop them. I tried. I tried so hard. But I wasn't strong enough. I wasn't good enough! I should have been able to stop them. I should... I should..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "So... my brother is a human being after all. This is going to be with you a long time, Jean-Luc. A long time." :quote/source "Robert Picard"}
     {:quote/quote "(using Picard's voice) One, seven, three, four, six, seven, three, two, one, four, seven, six, Charlie, three, two, seven, eight, nine, seven, seven, seven, six, four, three, Tango, seven, three, two, Victor, seven, three, one, one, seven, eight, eight, eight, seven, three, two, four, seven, six, seven, eight, nine, seven, six, four, three, seven, six. Lock." :quote/source "Data"}
     {:quote/quote "be a member of Starfleet. Nothing else mattered to me. Virtually my entire youth was spent in the pursuit of that goal. In fact...I probably skipped my childhood altogether." :quote/source "Picard: Ever since I was a child, I've always known exactly what I wanted to do"}
     {:quote/quote "You, have you ever been a father, Picard? Have you ever had a son desperately trying to win your approval, your respect? Jono broke his ribs riding on a t'stayan. Six hooves, a very powerful beast. The arm he broke in a competition with other youths. He endured the pain, and won the competition. One day he will be a great warrior." :quote/source "Endar"}
     {:quote/quote "Captain, is it worth it to go to war over a child?" :quote/source "Worf"}
     {:quote/quote "If there's nothing wrong with me... maybe there's something wrong with the universe." :quote/source "Beverly Crusher"}
     {:quote/quote "We will start with the assumption that I am not crazy." :quote/source "Beverly Crusher"}
     {:quote/quote "Computer. Read the entire crew roster for the Enterprise." :quote/source "Beverly Crusher"}
     {:quote/quote "Dr. Beverly Crusher." :quote/source "Computer"}
     {:quote/quote "Have I always been the only member of the crew on the starship Enterprise?" :quote/source "Beverly Crusher"}
     {:quote/quote "Affirmative." :quote/source "Computer"}
     {:quote/quote "If this were a bad dream, would you tell me?" :quote/source "Beverly Crusher"}
     {:quote/quote "That is not a valid question." :quote/source "Computer"}
     {:quote/quote "Like hell it's not. What is the primary mission of the starship Enterprise?" :quote/source "Beverly Crusher"}
     {:quote/quote "To explore the galaxy." :quote/source "Computer"}
     {:quote/quote "Do I have the necessary skills to complete that mission alone?" :quote/source "Beverly Crusher"}
     {:quote/quote "Negative." :quote/source "Computer"}
     {:quote/quote "Then why am I the only crew member?" :quote/source "Beverly Crusher"}
     {:quote/quote "[confused beeping]" :quote/source "Computer"}
     {:quote/quote "Aha. Got you there." :quote/source "Beverly Crusher"}
     {:quote/quote "What is the nature of the universe?" :quote/source "Beverly Crusher: Here's a question you shouldn't be able to answer...Computer"}
     {:quote/quote "The universe is a spheroid region 705 meters in diameter." :quote/source "Computer"}
     {:quote/quote "Click my heels together three times and I'm back in Kansas. Can it really be that simple?" :quote/source "Beverly Crusher"}
     {:quote/quote "[To Ishara Yar, on friendship] As I experience certain sensory input patterns, my mental pathways become accustomed to them. The inputs eventually are anticipated. And even missed when absent." :quote/source "Data"}
     {:quote/quote "You mean, you can become 'used' to someone.' :quote/source 'Ishara"}
     {:quote/quote "Exactly." :quote/source "Data"}
     {:quote/quote "[To Data, on trust] In all trust, there is the possibility for betrayal." :quote/source "William Riker"}
     {:quote/quote "I'm the only one, Worf, the only one who can prove your innocence. Kill me and you're a traitor forever." :quote/source "Duras"}
     {:quote/quote "Then that is how it shall be!" :quote/source "Worf (in fury)"}
     {:quote/quote "Shut up!" :quote/source "Captain Riker"}
     {:quote/quote "I beg your pardon?!" :quote/source "Admiral Picard"}
     {:quote/quote "I said shut up! As in close your mouth and stop talking!" :quote/source "Captain Riker"}
     {:quote/quote "You don't exactly look ship-shape yourself, sir." :quote/source "Wesley Crusher"}
     {:quote/quote "Therapists are always the worst patients. Except for doctors, of course." :quote/source "Beverly Crusher"}
     {:quote/quote "A resumption of our present course at warp six will place us in the T'lli Beta system in six days, thirteen hours, forty-seven minutes." :quote/source "Data"}
     {:quote/quote "What, no seconds?" :quote/source "William Riker"}
     {:quote/quote "I have discovered, sir, a certain level of impatience when I calculate a lengthy time interval to the nearest second. However, if you wish..." :quote/source "Data"}
     {:quote/quote "No, no. Minutes is fine." :quote/source "William Riker"}
     {:quote/quote "Is this how you handle all of your personnel problems?" :quote/source "Deanna Troi"}
     {:quote/quote "Sure. You'd be surprised how far a hug goes with Geordi, or Worf." :quote/source "William Riker"}
     {:quote/quote "I could be pursuing an untamed ornithoid without cause." :quote/source "Data"}
     {:quote/quote "[experimenting with 'friendly insults and jibes'] My hair does not require trimming, you lunkhead.' :quote/source 'Data"}
     {:quote/quote "[performing the tap step he has just learned] Am I dancing, Doctor?" :quote/source "Data"}
     {:quote/quote "They don't do a lot of tap-dancing at weddings." :quote/source "Beverly Crusher"}
     {:quote/quote "Why?" :quote/source "Data"}
     {:quote/quote "Well, Data, because...I don't really know why, Data." :quote/source "Beverly Crusher"}
     {:quote/quote "Some days you get the bear, and some days the bear gets you." :quote/source "William Riker"}
     {:quote/quote "You're a fool, Picard. History will look at you and say, 'This man was a fool.'' :quote/source 'Benjamin Maxwell"}
     {:quote/quote "I'll accept the judgment of history." :quote/source "Jean-Luc Picard"}
     {:quote/quote "It's not you I hate, Cardassian. I hate what I became because of you." :quote/source "Miles O'Brien"}
     {:quote/quote "The loyalty you would so quickly dismiss does not come easily to my people, Gul Macet. You have much to learn about us. Benjamin Maxwell earned the loyalty of those who served with him. In war, he was twice honored with the Federation's highest citation for his courage and valor. And if he could not find a role for himself in peace, we can pity him, but we shall not dismiss him." :quote/source "Jean-Luc Picard"}
     {:quote/quote '"We'll be watching.'' :quote/source 'Jean-Luc Picard: [turns his back on the Cardassian officer] Take this message to your leaders, Gul Macet"}
     {:quote/quote "In the hands of a con artist, fear can be used to motivate obedience, capitulation, the exploitation of innocent people - and that is what I believe has happened here and I intend to prove that." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I've encountered many who are more credibly to be called the devil than you." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Captain, I wasn't expecting you here." :quote/source "Geordi La Forge"}
     {:quote/quote "Neither was I." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Never mind, Mr. Worf, just have Commander Data fetch me in a shuttle. And have him bring a uniform." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Did you say... uniform?" :quote/source "Worf"}
     {:quote/quote "The advocate will refrain from making her opponent disappear." :quote/source "Data"}
     {:quote/quote "[after Ardra becomes the Devil] Any more disruptions and I will rule you in contempt of court. Is that understood?" :quote/source "Data"}
     {:quote/quote "I will draw my own conclusions, if you do not mind... Sir." :quote/source "Data"}
     {:quote/quote "I go home each night to a loving wife, two beautiful daughters. We eat the evening meal together as a family, I think that's important. And they always ask me if I've had a good day." :quote/source "Chancellor Durken"}
     {:quote/quote "And how shall you answer them tonight, Chancellor?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I will have to say this morning, I was the leader of the universe as I knew it. This afternoon, I am only a voice in a chorus. But I think it was a good day." :quote/source "Chancellor Durken"}
     {:quote/quote "We have to dream in order to survive." :quote/source "Beverly Crusher"}
     {:quote/quote "What is that?!" :quote/source "Gillespie"}
     {:quote/quote "Just a little souvenir I picked up on Makus III. That was setting number one. Anyone want to see setting number two?" :quote/source "Guinan"}
     {:quote/quote "As my final duty as acting captain, I order you to bed." :quote/source "Data"}
     {:quote/quote "Computer. Begin new program. Create as follows. Work station chair. Now, create a standard alphanumeric console positioned for the left hand. Now an iconic display console positioned for the right hand. Tie both consoles into the Enterprise main computer core utilizing neural scan interface." :quote/source "Reginald Barclay"}
     {:quote/quote "There is no such device on file." :quote/source "Computer"}
     {:quote/quote "No problem; here's how you build it." :quote/source "Reginald Barclay"}
     {:quote/quote "I perceive the entire universe as a single equation and it's so simple." :quote/source "Reginald Barclay"}
     {:quote/quote "Emotive, electrochemical stimulus response; cranial plate; bipedal locomotion. Endoskeletal. Contiguous external integument." :quote/source "Cytherian Alien"}
     {:quote/quote "I'm Captain Jean-Luc Picard, of the Federation Starship Enterprise." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Hierarchical collective command structure." :quote/source "Cytherian Alien"}
     {:quote/quote "Who are you?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Interrogative!" :quote/source "Cytherian Alien"}
     {:quote/quote "[To Picard] You are the most impossible person to buy a gift for!" :quote/source "Q"}
     {:quote/quote "I've just been paid a visit by Q." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Q? Any idea what he's up to?" :quote/source "William Riker"}
     {:quote/quote "He wants to do something nice for me." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I'll alert the crew." :quote/source "William Riker"}
     {:quote/quote "Sir, I protest! I am not a merry man!" :quote/source "Worf"}
     {:quote/quote "...Sorry." :quote/source "Worf"}
     {:quote/quote "Meet my new partner." :quote/source "Vash"}
     {:quote/quote "Him?!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Why not?" :quote/source "Vash"}
     {:quote/quote "I'll tell you why not...!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Now, Jean-Luc, let's not be unkind..." :quote/source "Q"}
     {:quote/quote "He's devious, and amoral, and unreliable, and irresponsible, and... and definitely not to be trusted." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Remind you of someone you know?" :quote/source "Vash"}
     {:quote/quote "(forced to smile) As a matter of fact, it does." :quote/source "Jean-Luc Picard"}
     {:quote/quote "The Federation does have enemies, we must seek them out!" :quote/source "Worf"}
     {:quote/quote "Oh yes, that's how it starts, but the road from legitimate suspicion to rampant paranoia is very much shorter than we think." :quote/source "Jean-Luc Picard"}
     {:quote/quote "You know, there are some words I've known since I was a schoolboy. 'With the first link, the chain is forged. The first speech censored, the first thought forbidden, the first freedom denied, chains us all irrevocably.' Those words were uttered by Judge Aaron Satie as wisdom and warning. The first time any man's freedom is trodden on, we're all damaged.' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "How dare you! You who consort with Romulans! Invoke my father's name in support of your traitorous arguments! It is an affront to everything I hold dear. And to hear his name used to subvert the United Federation of Planets. My father was a great man. His name stands for principle, and integrity! You dirty his name when you speak it! He loved the Federation! But you, Captain, corrupt it! You undermine our very way of life! I will expose you for what you are! I've brought down bigger men than you, Picard!" :quote/source "Adm. Norah Satie"}
     {:quote/quote "We think we've come so far. Torture of heretics, burning of witches, is all ancient history. Then, before you can blink an eye, suddenly, it threatens to start all over again." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I believed her. I... helped her. I did not see her for what she was." :quote/source "Worf"}
     {:quote/quote "Mr. Worf, villains who twirl their moustaches are easy to spot. Those who clothe themselves in good deeds are well-camouflaged." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I think... after yesterday people will not be so ready to trust her." :quote/source "Worf"}
     {:quote/quote "Maybe. But she, or someone like her, will always be with us. Waiting for the right climate in which to flourish, spreading fear in the name of righteousness. [...] Vigilance, Mr. Worf. That is the price we must continually pay." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Five hundred years ago, military officers would upend a drum in a battlefield, sit at it and dispense summary justice. Decisions were quick, punishments severe, appeals denied. Those who came to a drumhead were doomed." :quote/source "Picard"}
     {:quote/quote "We raise them, we care for them, we suffer for them, we keep them from harm their whole lives. Now eventually, it's their turn to take care of us." :quote/source "Lwaxana Troi"}
     {:quote/quote "No parent should expect to be paid back for the love they have given their children." :quote/source "Timicin"}
     {:quote/quote "Well, why the hell not?" :quote/source "Lwaxana Troi"}
     {:quote/quote "How about a game, Computer?" :quote/source "Geordi La Forge"}
     {:quote/quote "Please restate request." :quote/source "Computer"}
     {:quote/quote "Something to pass the time. Y'know, a diversion?" :quote/source "Geordi La Forge"}
     {:quote/quote "Select either 'visual interactive' or 'verbal interactive'.' :quote/source 'Computer"}
     {:quote/quote "Verbal." :quote/source "Geordi La Forge"}
     {:quote/quote "You have 20 seconds to respond to each question. Level of difficulty will increase as you progress. Proceed when ready." :quote/source "Computer"}
     {:quote/quote "Begin!" :quote/source "Geordi La Forge"}
     {:quote/quote "List the resonances of the sub-quantum states associated with transitional relativity..." :quote/source "Computer"}
     {:quote/quote "[Geordi laughs] That's easy!" :quote/source "Geordi La Forge"}
     {:quote/quote "...in alphabetical order." :quote/source "Computer"}
     {:quote/quote "Welcome, Mr. La Forge. We've waited a long time to meet you." :quote/source "Subcommander Taibak"}
     {:quote/quote "(strapped into a chair) I can tell you've gone to a lot of trouble." :quote/source "Geordi La Forge"}
     {:quote/quote "Indeed we have." :quote/source "Subcommander Taibak"}
     {:quote/quote "Report, please.  Geordi, please respond." :quote/source "Data"}
     {:quote/quote "Computer, current location of Lt. Commander La Forge." :quote/source "Data"}
     {:quote/quote "Cargo Bay 4." :quote/source "Computer"}
     {:quote/quote "Data to Lt. Worf.  Priority 1." :quote/source "Data"}
     {:quote/quote "Go ahead." :quote/source "Worf"}
     {:quote/quote "Take Commander La Forge into custody immediately." :quote/source "Data"}
     {:quote/quote "Sir?" :quote/source "Worf"}
     {:quote/quote "That is an order." :quote/source "Data"}
     {:quote/quote "Mr. Worf, get him out of here." :quote/source "Picard"}
     {:quote/quote "Harming our enemies is not enough!  Now the Federation would murder me to achieve its aims!" :quote/source "Vagh"}
     {:quote/quote "Governor, if I could explain this, I would." :quote/source "Picard"}
     {:quote/quote "I believe I can help, Captain.  I've been able to determine that Commander La Forge was abducted by Romulans en route to Rissa.  It is likely that he was somehow forced to take part in the plot to assassinate Governor Vagh." :quote/source "Data"}
     {:quote/quote "I saw no evidence of Romulans!  We just witnessed him acting very much alone." :quote/source "Ambassador Kell"}
     {:quote/quote "No, sir.  You did not.  I do not believe the Commander is acting out of his own accord.  He has been receiving heat band signals through his VISOR, signals which are carrying direct commands to his brain.  I have surmised that Commander La Forge was conditioned by Romulans, a process referred to historically, and somewhat inaccurately, as 'brainwashing.'' :quote/source 'Data"}
     {:quote/quote "But to what end?  Why would the Romulans want to kill me?" :quote/source "Vagh"}
     {:quote/quote "The Romulans have always wanted to destroy the alliance between the Federation and the Klingons.  If Mr. La Forge had killed you, Governor, I think you will agree that they might have succeeded." :quote/source "Picard"}
     {:quote/quote "Who sent these signals?  A cloaked Romulan ship?" :quote/source "Kell"}
     {:quote/quote "No, sir.  That is not possible.  The signals had to be transmitted within close proximity to the VISOR." :quote/source "Data"}
     {:quote/quote "Are you suggesting that there was a Romulan accomplice who was in close proximity to Mr. La Forge when he was receiving signals?" :quote/source "Picard"}
     {:quote/quote "Yes, sir.  I am." :quote/source "Data"}
     {:quote/quote "This Romulan accomplice.  Who is he?" :quote/source "Vagh"}
     {:quote/quote "Captain Picard and Ambassador Kell.  One of them may be concealing a heat band transmitter.  If they would agree to be searched..." :quote/source "Data: I have narrowed the list of possibilities to two people.  The only two people who were with Mr. La Forge all three times when the transmission was recorded"}
     {:quote/quote "I am a Klingon!  An Emissary of the High Council!  I will not submit to being forced to be searched by you or anyone else o on this ship!" :quote/source "Kell"}
     {:quote/quote "I am forced to agree, Captain." :quote/source "Vagh's Guard"}
     {:quote/quote "We will take the ambassador with us and search him ourselves." :quote/source "Vagh"}
     {:quote/quote "Captain, I believe it to be in all our best interests if I remain aboard.  I formally request asylum." :quote/source "Kell"}
     {:quote/quote "I will certainly grant you asylum...when you have been absolved of this crime." :quote/source "Picard"}
     {:quote/quote "I wish we were back there now, you and I." :quote/source "Jenna D'Sora"}
     {:quote/quote "The unidirectional nature of the time continuum makes that an unlikely possibility." :quote/source "Data"}
     {:quote/quote "Listen, my advice is... ask somebody else for advice, at least someone who's got more experience at... giving advice." :quote/source "Geordi La Forge"}
     {:quote/quote "Ultimately Jenna will care for you for who you are, not what you imitate out of a book." :quote/source "Deanna Troi"}
     {:quote/quote "My programming may be inadequate to the task." :quote/source "Data"}
     {:quote/quote "Klingons do not... pursue relationships. They conquer that which they desire." :quote/source "Worf"}
     {:quote/quote "However, Lieutenant D'Sora serves under my command. If she were.. mistreated, I would be very displeased... ...Sir." :quote/source "Worf"}
     {:quote/quote "I understand." :quote/source "Data"}
     {:quote/quote "I'd be delighted to offer any advice I have on understanding women. When I have some, I'll let you know." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Computer, decrease illumination level by 1/3 standard lux." :quote/source "Data"}
     {:quote/quote "Eh, this is all part of a programme?" :quote/source "Jenna D'Sora"}
     {:quote/quote "Yes, one which I have just created for romantic relationships" :quote/source "Data"}
     {:quote/quote "So, I am just a small variable in one of you computational environments." :quote/source "Jenna D'Sora"}
     {:quote/quote "You are much more than that, Jenna. I have written a subroutine specifically for you. A programme within the programme. I have devoted a considerable share of my internal resources to its development." :quote/source "Data"}
     {:quote/quote "Data, that's the nicest thing anybody has ever said to me." :quote/source "Jenna D'Sora"}
     {:quote/quote "Jenna, are we no longer a couple?" :quote/source "Data"}
     {:quote/quote "No, we're not." :quote/source "Jenna D'Sora"}
     {:quote/quote "Then I will delete the appropriate program." :quote/source "Data"}
     {:quote/quote "Defeat." :quote/source "B'Etor"}
     {:quote/quote "How? Where are the Romulans?" :quote/source "Toral"}
     {:quote/quote "They never came." :quote/source "Lursa"}
     {:quote/quote "Kill him." :quote/source "Lursa"}
     {:quote/quote "NO!" :quote/source "Toral"}
     {:quote/quote "Toral, the next Leader of the Empire. Gowron is looking forward to seeing you again." :quote/source "Kurn"}
     {:quote/quote "Rai and Jiri at Lungha. Ri of Luwani. Luwani under two moons. Jiri of Ubaya. Ubaya of crossed roads at Lungha. Lungha, her sky grey. [greeting]" :quote/source "Dathon"}
     {:quote/quote "Kadir beneath Mo Moteh. [cluelessness, possibly unwillingness to understand]" :quote/source "Tamarian lst Officer"}
     {:quote/quote "The river Temarc. In winter. [Stop, cease]" :quote/source "Dathon"}
     {:quote/quote "Shaka. When the walls fell. [Failure]" :quote/source "Dathon"}
     {:quote/quote "Mirab. His sails unfurled. [travel or departure]" :quote/source "Tamarian lst Officer"}
     {:quote/quote "Darmok and Jalad at Tenagra. [Two strangers uniting against a common adversary]" :quote/source "Dathon"}
     {:quote/quote "Temba. His arms wide. [A gift, or to give]" :quote/source "Dathon"}
     {:quote/quote "Uzani. His army with fist open." :quote/source "Dathon"}
     {:quote/quote "A strategy? With fist open? With fist open..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "His army with fist closed." :quote/source "Dathon"}
     {:quote/quote "With fist closed? An army with fist open... to lure the enemy. With fist closed... To attack? That's how you communicate, isn't it? By citing example. By metaphor! Uzani's army with... with fist open." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Sokath! His eyes uncovered! [understanding, revealing truth]" :quote/source "Dathon"}
     {:quote/quote "Darmok on the ocean. [Alone, isolated]" :quote/source "Dathon"}
     {:quote/quote "[telling the story of Darmok and Jalad]" :quote/source "Dathon"}
     {:quote/quote "Kiazi's children. Their faces wet. [death, mourning]" :quote/source "Dathon"}
     {:quote/quote "Zinda! His face black. His eyes red. [war, violence] Callimas at Bahar." :quote/source "Dathon"}
     {:quote/quote '"Send us a companion for our king! Spare us from his madness!' Enkidu, a wild man from the forest, entered the city. They fought in the temple. They fought in the streets. Gilgamesh defeated Enkidu. They became great friends. Gilgamesh and Enkidu at Uruk.' :quote/source 'Jean-Luc Picard: [paraphrasing The Epic of Gilgamesh] Gilgamesh, a king. Gilgamesh, a king at Uruk. [metaphor about ancient Babylonian legends from Earth] He tormented his subjects. He made them angry. They cried out aloud"}
     {:quote/quote "At Uruk." :quote/source "Dathon"}
     {:quote/quote "The... the new friends went out into the desert together, where the Great Bull of Heaven was killing men by the hundreds. Enkidu caught the bull by the tail, Gilgamesh struck him with his sword." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Gilgamesh." :quote/source "Dathon"}
     {:quote/quote "They were victorious. But... Enkidu fell to the ground, struck down by the gods; and Gilgamesh wept bitter tears, saying, 'He who was my companion through adventure and hardship, [as Dathon dies] is gone forever.'' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "Zinda! His face black, his eyes red!" :quote/source "Tamarian 1st Officer"}
     {:quote/quote "Temarc! The river Temarc! In winter!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Darmok..." :quote/source "Tamarian 1st Officer"}
     {:quote/quote "And Jalad. At Tanagra. Darmok and Jalad... on the ocean." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Sokath! His eyes opened!" :quote/source "Tamarian 1st Officer"}
     {:quote/quote "The beast at Tanagra? Uzani. His army. Shaka, when the walls fell." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Picard and Dathon at El-Adrel. [a new metaphor added to the language]" :quote/source "Tamarian 1st Officer"}
     {:quote/quote "[to the crew] Mirab with sails unfurled." :quote/source "Tamarian lst Officer"}
     {:quote/quote "[offering Dathon's knife] Temba. His arms wide." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Temba. At rest. [You may keep it.]" :quote/source "Tamarian 1st Officer"}
     {:quote/quote "Thank you." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Yes, Ensign Laren, please sit down -" :quote/source "Picard"}
     {:quote/quote "Ensign Ro, sir." :quote/source "Ensign Ro"}
     {:quote/quote "(nonplussed) I beg your pardon?" :quote/source "Picard"}
     {:quote/quote "The Bajoran custom has the family name first, the individual's name second. I am more properly addressed as 'Ensign Ro'." :quote/source "Ensign Ro"}
     {:quote/quote "I'm sorry, I didn't know." :quote/source "Picard"}
     {:quote/quote "(shrugging) No, there's no reason you should. It's an old custom; most Bajora these days accept the distortion of their names in order to assimilate. (she sits) I do not." :quote/source "Ensign Ro"}
     {:quote/quote "I cannot condone violence against people who are not our enemy." :quote/source "Keeve"}
     {:quote/quote "Then I don't understand why you're unwilling [to help us]." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Because... you're innocent bystanders. You were innocent bystanders for decades as the Cardassians took our homes. As they violated and tortured our people in the most hideous ways imaginable. As we were forced to flee." :quote/source "Keeve"}
     {:quote/quote "We were saddened by those events, but they occurred within the borders of the Cardassian Empire." :quote/source "Picard"}
     {:quote/quote "And the Federation is pledged not to interfere with the internal affairs of others. How convenient that must be for you. To turn a deaf ear to those who suffer behind a line on a map." :quote/source "Keeve"}
     {:quote/quote "Well, I'm not here to debate Federation policy with you, but I can offer you assistance." :quote/source "Picard"}
     {:quote/quote "Simply because of one terrorist attack? Well, perhaps I should have known, we should have attacked the Federation long ago." :quote/source "Keeve"}
     {:quote/quote "We live in different universes, you and I. Yours is about diplomacy, politics, strategy. Mine is about blankets! If we were to exchange places for one night, you might better understand." :quote/source "Keeve"}
     {:quote/quote "Mr. Data, see that the replicators provide a blanket for every man, woman, and child before nightfall." :quote/source "Picard"}
     {:quote/quote "Am I disturbing you?" :quote/source "Guinan"}
     {:quote/quote "Yes." :quote/source "Ensign Ro"}
     {:quote/quote "Good. You look like someone who wants to be disturbed." :quote/source "Guinan"}
     {:quote/quote "I believe truth is in the eye of the beholder." :quote/source "Guinan"}
     {:quote/quote "Isn't that supposed to be beauty?" :quote/source "Ensign Ro"}
     {:quote/quote "Truth, beauty, works for a lot of things." :quote/source "Guinan"}
     {:quote/quote "You're not like any bartender I've met before" :quote/source "Ro"}
     {:quote/quote "And you're not like any Starfleet officer I've met before. But that sounds like the beginning of a very interesting friendship." :quote/source "Guinan"}
     {:quote/quote "I don't stay anywhere long enough to make friends." :quote/source "Ro"}
     {:quote/quote "Too late. You just did." :quote/source "Guinan"}
     {:quote/quote "[Touches comm badge] Bridge, this is Picard. This is the captain, does anyone read me?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "[crying] Why don't they answer?!" :quote/source "Child 1"}
     {:quote/quote "I don't know." :quote/source "Picard"}
     {:quote/quote "They're all dead." :quote/source "Child 2"}
     {:quote/quote "They're not dead, communication is down, that's all." :quote/source "Picard"}
     {:quote/quote "We're going to die, too." :quote/source "Child 2"}
     {:quote/quote "Congratulations. You are now fully dilated to ten centimeters. You may now give birth." :quote/source "Worf"}
     {:quote/quote "That's what I've been doing!" :quote/source "Keiko O'Brien"}
     {:quote/quote "The computer simulation was not like this. That delivery was very orderly." :quote/source "Worf"}
     {:quote/quote "WELL I'M SORRY!!" :quote/source "Keiko"}
     {:quote/quote "Push, Keiko! Push! Push!" :quote/source "Worf"}
     {:quote/quote "I AM PUSHING!" :quote/source "Keiko"}
     {:quote/quote "These quarters were obviously intended for one crewman, sir. There is only one sleeping space." :quote/source "Data"}
     {:quote/quote "I'm sure the Klingons found it amusing to put us in here together." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Since I do not require sleep, I propose you take the... shelf, sir. I am content to stand." :quote/source "Data"}
     {:quote/quote "[to Picard] In your own way, you are as stubborn as another Captain of the Enterprise I once knew." :quote/source "Spock"}
     {:quote/quote "Then I'm in good company, sir." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Fascinating. You have an efficient intellect, superior physical skills, no emotional impediments. There are Vulcans who aspire all their lives to achieve what you've been given by design." :quote/source "Spock"}
     {:quote/quote "You are half human?" :quote/source "Data"}
     {:quote/quote "Yes." :quote/source "Spock"}
     {:quote/quote "Yet you have chosen a Vulcan way of life?" :quote/source "Data"}
     {:quote/quote "I have." :quote/source "Spock"}
     {:quote/quote "In effect, you have abandoned what I have sought all my life." :quote/source "Data"}
     {:quote/quote "Ambassador Spock, may I ask a personal question?" :quote/source "Data"}
     {:quote/quote "Please." :quote/source "Spock"}
     {:quote/quote "As you examine your life, do you find you have missed your humanity?" :quote/source "Data"}
     {:quote/quote "I have no regrets." :quote/source "Spock"}
     {:quote/quote '"No regrets'. That is a human expression.' :quote/source 'Data"}
     {:quote/quote "Yes. Fascinating." :quote/source "Spock"}
     {:quote/quote "[on Worf and Amarie singing in Klingon] What is that dreadful noise?! It sounds like a Bardakian pronghorn moose!" :quote/source "Omag"}
     {:quote/quote "I will not read this, or any other statement." :quote/source "Spock"}
     {:quote/quote "If you do not you will die. All of you will die." :quote/source "Sela"}
     {:quote/quote "Since it is logical to conclude that you will kill us in any event, I choose not to cooperate." :quote/source "Spock"}
     {:quote/quote "I hate Vulcans." :quote/source "Sela"}
     {:quote/quote "I assume your handprint will open this door whether you are conscious or not." :quote/source "Data"}
     {:quote/quote '"Data! Data, isn't this exciting? We are going to witness a moment in history!'' :quote/source 'Geordi La Forge"}
     {:quote/quote '"Every nanosecond in this continuum is a moment in history, once it has elapsed.'' :quote/source 'Data (puzzled)"}
     {:quote/quote "I have often wished to be human. I study people carefully, in order to more closely approximate human behavior." :quote/source "Data"}
     {:quote/quote "Why? We're smarter and stronger than humans, we can do more than they can." :quote/source "Timothy"}
     {:quote/quote "But I cannot take pride in my abilities. I cannot take pleasure in my accomplishments." :quote/source "Data"}
     {:quote/quote "We never have to feel bad, either." :quote/source "Timothy"}
     {:quote/quote "I would gladly risk feeling bad at times, if it also meant that I could taste my dessert." :quote/source "Data"}
     {:quote/quote "It has been three centuries since anyone was treated for this... this form of rape. But there are medical records from that era. It was a time of great violence among my people. A time we thought we had put far behind us. That this could happen now... it's unimaginable." :quote/source "Tarmin"}
     {:quote/quote "Earth was once a violent planet, too. At times the chaos threatened the very fabric of life, but, like you, we evolved. We found better ways to handle our conflicts. But I think no one can deny that the seed of violence remains within each of us. We must recognize that, because that violence is capable of consuming each of us, as it consumed your son." :quote/source "Jean-Luc Picard"}
     {:quote/quote "The bartender is an artificial lifeform." :quote/source "Deanna Troi"}
     {:quote/quote "Can I get you something? A beverage?" :quote/source "Data"}
     {:quote/quote "Contact the operations officer to assist you." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Aye, sir." :quote/source "Geordi La Forge"}
     {:quote/quote "He's in Ten-Forward... waiting tables." :quote/source "Ensign Ro"}
     {:quote/quote "Well done, Captain." :quote/source "McDuff"}
     {:quote/quote "[To Dr Russell, who has been using experimental techniques to treat patients] You scare me, doctor. You risk your patients' lives and justify it in the name of research. Genuine research takes time... sometimes a lifetime of painstaking, detailed work in order to get any results. Not for you, you take short cuts, right through living tissue. You put your research ahead of your patients' lives and as far as I'm concerned, that's a violation of our most sacred trust. I'm sure your work will be hailed as a stunning breakthrough. Enjoy your laurels, doctor. I'm not sure I could." :quote/source "Beverly Crusher"}
     {:quote/quote "I am female. I was born that way. I have had those feelings… those longings… all my life. It is not unnatural. I am not sick because I feel this way. I do not need to be helped, and I do not need to be cured. What I do need — what all of those like me need — is your understanding and your compassion. We do not injure you in any way. And yet we are scorned, and attacked. And all because we are different. What we do is no different from what you do. We talk and laugh… we complain about work and we wonder about growing old… we talk about our families, and we worry about the future…We cry with each other when things seem hopeless. All the loving things that you do with each other… that’s what we do. And for that, we are called misfits, and deviants… and criminals. What right do you have to punish us? What right do you have to change us? What makes you think you can dictate how people love each other?" :quote/source "Soren"}
     {:quote/quote "Sometimes I wonder if he's stacking the deck." :quote/source "William Riker"}
     {:quote/quote "I assure you, commander, the cards have been sufficiently randomized." :quote/source "Data"}
     {:quote/quote "[surly] I hope so." :quote/source "Worf"}
     {:quote/quote "I am experiencing nIb'poH, the feeling I have done this before." :quote/source "Worf"}
     {:quote/quote "Yes, last Tuesday night." :quote/source "Riker"}
     {:quote/quote "That's not what I mean." :quote/source "Worf"}
     {:quote/quote "My aunt Adelle cured a lot of sleepless nights with this steaming milk." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Thank you. Mmm, nutmeg." :quote/source "Beverly Crusher"}
     {:quote/quote "Whenever I get insomnia, I try to perfect the recipe." :quote/source "Picard"}
     {:quote/quote "Thank you, for everything." :quote/source "Beverly Crusher"}
     {:quote/quote "Uh uh. Thank Aunt Adelle." :quote/source "Picard"}
     {:quote/quote "The first duty of every Starfleet officer is to the truth, whether it's scientific truth or historical truth or personal truth! It is the guiding principle on which Starfleet is based! If you can't find it within yourself to stand up and tell the truth about what happened, you don't deserve to wear that uniform!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Why are they arguing?" :quote/source "Alexander Rozhenko"}
     {:quote/quote "They're friends. They love contradiction. They they thrive on challenge! They flourish in conflict." :quote/source "Juggler"}
     {:quote/quote "Then why are they friends?" :quote/source "Alexander Rozhenko"}
     {:quote/quote "Who else are you going to argue with if not your friends?" :quote/source "Lwaxana Troi"}
     {:quote/quote "I wish I knew how I could help." :quote/source "Beverly Crusher"}
     {:quote/quote "Perhaps I just needed a shoulder." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Well it's there for you, Jean-Luc. It always has been." :quote/source "Beverly Crusher"}
     {:quote/quote "No, it's a Samalian coral fish with its fin unfolded." :quote/source "Guinan"}
     {:quote/quote "I believe what you are seeing is the effect of the fluid dynamic processes inherent in the large scale motion of rarefied gas." :quote/source "Data"}
     {:quote/quote "No, no. First it was a fish and now it's a Mentonian sailing ship." :quote/source "Guinan"}
     {:quote/quote "Where?" :quote/source "Data"}
     {:quote/quote "Right there. Don't you see the two swirls coming together to form the mast?" :quote/source "Guinan"}
     {:quote/quote "I do not see it. It is interesting that people try to find meaningful patterns in things that are essentially random. I have noticed that the images they perceive sometimes suggest what they are thinking about at that particular moment. Besides, it is clearly a bunny rabbit." :quote/source "Data"}
     {:quote/quote "[to a disengaged Borg male teen, in the manner of 'Locutus'] This is a primitive culture. I am here to facilitate its incorporation.' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "But even in war there are rules. You don't kill civilians indiscriminately." :quote/source "Beverly Crusher"}
     {:quote/quote "There are no civilians among the Borg." :quote/source "William T. Riker"}
     {:quote/quote "We are Hugh." :quote/source "Hugh"}
     {:quote/quote "Resistance is not futile?" :quote/source "Hugh"}
     {:quote/quote "Part of what we do is learn more about other species." :quote/source "Geordi La Forge"}
     {:quote/quote "We assimilate species. Then we know everything about them." :quote/source "Hugh"}
     {:quote/quote "I don't have all the answers; I've never been dead before." :quote/source "Ro Laren"}
     {:quote/quote "I never knew what a friend was until I met Geordi. He spoke to me as though I were human. He treated me no differently from anyone else. He accepted me for what I am. And that, I have learned, is friendship. But I do not know how to say goodbye." :quote/source "Data"}
     {:quote/quote "1 June 1992)" :quote/source " Screenplay by Morgan Gendel and Peter Allan Fields. This episode won the 1993 Hugo Award for Best Dramatic Presentation. The younger Batai, the son of Kamin, was played by Patrick Stewart's real-life son, Daniel. (Stardate: 45944.1, original airdate"}
     {:quote/quote "[to Jean-Luc Picard] You think that this — your life — is a dream?" :quote/source "Eline"}
     {:quote/quote "This is not my life! I know that much." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Would you try to answer some questions for me? No matter how strange they may seem to you?" :quote/source "Picard"}
     {:quote/quote "Of course." :quote/source "Eline"}
     {:quote/quote "The happiest day of my life was the day we got married." :quote/source "Eline"}
     {:quote/quote "And what do I do — here in Ressik?" :quote/source "Picard"}
     {:quote/quote "You are the best iron weaver in the community — at least I think so. You prefer playing the flute, of course." :quote/source "Eline"}
     {:quote/quote "The flute?" :quote/source "Picard"}
     {:quote/quote "Yes. [she gets up, finds his flute and hands it to him]" :quote/source "Eline"}
     {:quote/quote "When did I learn to play this?" :quote/source "Picard"}
     {:quote/quote "I'm afraid you never did, dear. You do keep trying." :quote/source "Eline"}
     {:quote/quote "[blows a few feeble notes] I see what you mean. … Well, thank you for the soup. Thank you for your help. Tomorrow, will you help me send a message?" :quote/source "Picard"}
     {:quote/quote "Of course. Will you come to bed?" :quote/source "Eline"}
     {:quote/quote "Oh, I'll sleep here." :quote/source "Picard"}
     {:quote/quote "Kamin, please come with me." :quote/source "Eline"}
     {:quote/quote "I've been sick. I'd be tossing and turning, it wouldn't be fair to you." :quote/source "Picard"}
     {:quote/quote "Let me be the judge of that." :quote/source "Eline"}
     {:quote/quote "I think you're still trying to figure out where you are. Where that ship of yours is. How to get back to that life." :quote/source "Eline"}
     {:quote/quote "The memory is five years old now, but it's still inside me." :quote/source "Picard/Kamin"}
     {:quote/quote "Was your life there so much better than this? So much more gratifying, so much more fulfilling, that you cling to it with such stubbornness?" :quote/source "Eline"}
     {:quote/quote "Eline." :quote/source "Picard/Kamin"}
     {:quote/quote "It must have been extraordinary. But never in all the stories you've told me have you mentioned anyone who loved you as I do." :quote/source "Eline"}
     {:quote/quote "It was real. It was as real as this is. And you can't expect me to forget a life-time spent there." :quote/source "Picard/Kamin"}
     {:quote/quote "Yes, I can. I've been patient Kamin. For five years I've shared you with that other life. I’ve listened, I’ve tried to understand, and I have waited. When do I get you back?" :quote/source "Eline"}
     {:quote/quote "This tree is our symbol. Our affirmation of Life, and everyone in this town gives part of their water rations to keep it alive. We've learned, administrator, that hope is a powerful weapon against anything, even drought." :quote/source "Batai"}
     {:quote/quote "You've taught me to pursue the truth, no matter how painful it is. It's too late to back off now. This planet is dying." :quote/source "Meribor"}
     {:quote/quote "Perhaps I should have filled your head with trivial concerns. Games and toys and clothes." :quote/source "Picard/Kamin"}
     {:quote/quote "I don't think you mean that." :quote/source "Meribor"}
     {:quote/quote "No, I don't. It just saddens me to see you burdened with the knowledge of things you can't change." :quote/source "Picard/Kamin"}
     {:quote/quote "Father, I think I should marry Dannick sooner rather than later, don't you?" :quote/source "Meribor"}
     {:quote/quote "Seize the time, Meribor. Live now. Make now always the most precious time. Now will never come again." :quote/source "Picard/Kamin"}
     {:quote/quote "We hoped our probe would encounter someone in the future. Someone who could be a teacher. Someone who could tell the others about us." :quote/source "Batai"}
     {:quote/quote "Oh, it's me, isn't it? I'm the someone. I'm the one it finds. That's what this launching is. A probe that finds me in the future." :quote/source "Picard/Kamin"}
     {:quote/quote "Yes, my love." :quote/source "Eline"}
     {:quote/quote "Eline." :quote/source "Picard/Kamin"}
     {:quote/quote "The rest of us have been gone for a thousand years. If you remember what we were, and how we lived, then we'll have found life again." :quote/source "Eline"}
     {:quote/quote "Eline…" :quote/source "Picard/Kamin"}
     {:quote/quote "[the rocket rises into the sky] Now we live in you. Tell them of us, my darling." :quote/source "Eline"}
     {:quote/quote "[referring to disembodied head] Data, is this yours?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "I believe so, sir." :quote/source "Data"}
     {:quote/quote "Could it be... Lore?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "No, sir. My brother's positronic brain has a Type-L phase discriminating amplifier. Mine is a Type-R." :quote/source "Data"}
     {:quote/quote "Have you ever heard Data define friendship?" :quote/source "Counselor Deanna Troi"}
     {:quote/quote "No." :quote/source "Commander William T. Riker"}
     {:quote/quote "How did he put it? 'As I experience certain sensory input patterns, my mental pathways become accustomed to them. The inputs eventually are anticipated and even missed when absent.'' :quote/source 'Troi"}
     {:quote/quote "It's just that our mental pathways have become accustomed to your sensory input patterns." :quote/source "Riker"}
     {:quote/quote "Hmm, I understand. I am also fond of you, Commander. And you as well, Counselor." :quote/source "Data"}
     {:quote/quote "Just what are you doing with those lamps?" :quote/source "Doctor"}
     {:quote/quote "Replacing the burners. City ordinance. Makes it safer in case of earthquake." :quote/source "Jean-Luc Picard"}
     {:quote/quote "There hasn't been an earthquake here in thirty years!" :quote/source "Doctor"}
     {:quote/quote "Well, that takes care of this ward. Time to be moving on." :quote/source "Jean-Luc Picard"}
     {:quote/quote "(scoffs) Earthquakes..." :quote/source "Doctor"}
     {:quote/quote "Barclay, I'm gonna need a systems engineer on this away team." :quote/source "Geordi La Forge"}
     {:quote/quote "I'll, I'll ask Ensign Dern to join you." :quote/source "Reginald Barclay"}
     {:quote/quote "I meant you, Barclay!" :quote/source "La Forge"}
     {:quote/quote "You're not quite uh...human, are ya?" :quote/source "Scotty"}
     {:quote/quote "No, sir. I am an android. Lieutenant Commander Data." :quote/source "Data"}
     {:quote/quote "[inspecting his drink] What is it?" :quote/source "Scotty"}
     {:quote/quote "It is... [sniffs the bottle] it is... [sniffs the bottle again, looking puzzled] ...it is green. (An echo of Scotty's comment to the Andromedan/Kelvan alien Tomar in TOS' By Any Other Name)" :quote/source "Data"}
     {:quote/quote "Please enter program." :quote/source "Computer"}
     {:quote/quote "The android at the bar said you could show me my old ship, let me see it." :quote/source "Scotty"}
     {:quote/quote "Insufficient data. Please specify parameters." :quote/source "Computer"}
     {:quote/quote "The Enterprise, show me the bridge of the Enterprise, ya chattering piece of-" :quote/source "Scotty"}
     {:quote/quote "There have been five Federation ships with that name. Please specify by registry number." :quote/source "Computer"}
     {:quote/quote "NCC-1701. No bloody A, B, C, or D." :quote/source "Scotty"}
     {:quote/quote "Program complete. Enter when ready." :quote/source "Computer"}
     {:quote/quote "[pours a drink and gives a toast] Here's to ya, lads." :quote/source "Scotty"}
     {:quote/quote "(reciting 'Ode To Spot')" :quote/source "Data"}
     {:quote/quote "Your hesitation suggests you are trying to protect my feelings. However, since I have none, I would prefer you to be honest." :quote/source "Data"}
     {:quote/quote "[after yawning] Oh, excuse me." :quote/source "La Forge"}
     {:quote/quote "I know the feeling." :quote/source "Riker"}
     {:quote/quote "Good morning." :quote/source "La Forge"}
     {:quote/quote "Morning?! I just went to bed!" :quote/source "Riker"}
     {:quote/quote "Commander, it's 0700 hours." :quote/source "La Forge"}
     {:quote/quote "Have you dreamed about scissors recently?" :quote/source "Troi"}
     {:quote/quote "Whoever it was that sent that thing was more than simply curious..." :quote/source "Riker"}
     {:quote/quote "I've been in this room before." :quote/source "La Forge"}
     {:quote/quote "We've all been here before." :quote/source "Riker"}
     {:quote/quote "Everything was normal and then, suddenly it's like the laws of physics went right out the window." :quote/source "Geordi La Forge"}
     {:quote/quote "And why shouldn't they?  They're so inconvenient." :quote/source "Q"}
     {:quote/quote "Are you responsible for this incident in engineering?" :quote/source "Picard"}
     {:quote/quote "Of course! I needed to find out if what I suspected about the girl were true." :quote/source "Q"}
     {:quote/quote "That being..." :quote/source "Picard"}
     {:quote/quote "That she's Q." :quote/source "Q"}
     {:quote/quote "Crusher gets more shrill with every passing year." :quote/source "Q"}
     {:quote/quote "You don't have to eat, you know.  It's a nasty human habit you could do without." :quote/source "Q"}
     {:quote/quote "Jean-Luc, sometimes I think the only reason I come here is to listen to these wonderful speeches of yours..." :quote/source "Q"}
     {:quote/quote "With unlimited power comes responsibility." :quote/source "Q"}
     {:quote/quote "What is it about these squirming little infants that you find so appealing?" :quote/source "Q"}
     {:quote/quote "I'm sure that's beyond your comprehension, Q." :quote/source "Beverly Crusher"}
     {:quote/quote "I desperately hope so." :quote/source "Q"}
     {:quote/quote "Ever since I got here, I've been fighting this… I've been denying the truth… denying what I am. I am Q." :quote/source "Amanda Rogers"}
     {:quote/quote "You were on one of the most beautiful planets in the entire quadrant, and you spent the entire time in a cave?" :quote/source "Guinan"}
     {:quote/quote "(nodding happily) It was a very rewarding experience. Look at these fragments! They're very nearly in perfect condition, and yet they're seven hundred years old." :quote/source "Picard"}
     {:quote/quote "So's my father." :quote/source "Guinan"}
     {:quote/quote "[in Texan accent] Howdy, commander!" :quote/source "Data"}
     {:quote/quote "You just sit tight.  We'll have this all fixed up in time for supper." :quote/source "Data"}
     {:quote/quote "The town of Deadwood may face danger once again.  If they do, they will need a sheriff...and a deputy." :quote/source "Worf"}
     {:quote/quote "I have always been a little suspicious of men in beards." :quote/source "Beverly Crusher"}
     {:quote/quote "My beard is not an affectation!" :quote/source "Riker"}
     {:quote/quote "Oh, and get that fish out of the ready room." :quote/source "Edward Jellico"}
     {:quote/quote "Data, I want to be in Minos Korva... in one hour." :quote/source "Edward Jellico"}
     {:quote/quote "Set course 350 mark 215 and engage warp 8.5." :quote/source "Data"}
     {:quote/quote "I remember the first time I ate a live taspar. I was six years old and living on the streets of Lakat. There was a band of children, four, five...six years old—some even smaller, desperately trying to survive. We were thin, scrawny little animals, constantly hungry, always cold. We slept together in doorways, like packs of wild gettles, for warmth. Once I found a nest. Taspars had mated and built a nest in the eave of a burned-out building. And I found three eggs in it. It was like finding treasure. I cracked one open on the spot and ate it, very much as you just did. I planned to save the other two. They would keep me alive for another week. But of course, an older boy saw them and wanted them. And he got them. But he had to break my arm to do it." :quote/source "Gul Madred"}
     {:quote/quote "Must be rewarding to you to...to repay others for all those years of misery." :quote/source "Jean-Luc Picard"}
     {:quote/quote "What do you mean?" :quote/source "Gul Madred"}
     {:quote/quote "Torture has never been a reliable means of extracting information. It is ultimately self-defeating as a means of control. One wonders it is still practiced." :quote/source "Jean-Luc Picard"}
     {:quote/quote "I fail to see where this analysis is leading." :quote/source "Gul Madred"}
     {:quote/quote "Whenever I look at you now, I will not see a powerful cardassian warrior; I will see a six-year-old boy who is powerless to protect himself." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Be quiet!" :quote/source "Gul Madred"}
     {:quote/quote "In spite of all you have done to me, I find you a pitiable man." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Picard, stop it. Or I will turn this on and leave you in agony all night!" :quote/source "Gul Madred"}
     {:quote/quote "Aha! You called me 'Picard!'' :quote/source 'Jean-Luc Picard"}
     {:quote/quote "What are the Federation's defense plans for Minos Korva?" :quote/source "Gul Madred"}
     {:quote/quote "There are four lights!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Let's drop ranks for a moment. I don't like you. I believe you're arrogant, insubordinate, willful. I don't think you're a particularly good First Officer." :quote/source "Edward Jellico"}
     {:quote/quote "Well, now that the ranks are dropped, Captain...I don't like you, either. You ARE arrogant, and closed-minded. You need to control everyone and everything. You don't provide an atmosphere of trust, and you don't inspire these people to go out of their way for you. You've got everyone wound up so tight, there's no joy in anything. I don't think you're a particularly good Captain." :quote/source "William Riker"}
     {:quote/quote "We acquired territory during the wars... we developed new resources... we initiated a rebuilding program... we have mandated agricultural programs. That is what the military has done for Cardassia. Because of that... my daughter will never have to worry about going hungry." :quote/source "Gul Madred"}
     {:quote/quote "Her belly may be full... but her spirit will be empty." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Be careful... you're showing weakness..." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Shall we begin again? How many lights are there?" :quote/source "Gul Madred"}
     {:quote/quote "What lights?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "There...are...four...lights!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "between a life of comfort...or more torture. All I had to do was say that...I could see five lights, when in fact there were only four." :quote/source "Jean-Luc Picard: At the end, he gave me a choice"}
     {:quote/quote "You didn't say it." :quote/source "Deanna Troi"}
     {:quote/quote "No. No. But I was going to. I would have told him anything. Anything at all. But more than that—I believed that I could see...five lights." :quote/source "Jean-Luc Picard"}
     {:quote/quote "(Shakes Picard's hand) Welcome to the Afterlife Jean-Luc...You're dead!" :quote/source "Q"}
     {:quote/quote "You're dead, this is the afterlife...and I'm God" :quote/source "Q"}
     {:quote/quote "You are not God!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Blasphemy! You're lucky I don't cast you out or smite you or something. The bottom line is, your life ended about five minutes ago... under the inept ministrations of Dr. Beverly Crusher." :quote/source "Q"}
     {:quote/quote "No. I am not dead. Because I refuse to believe that the afterlife is run by you. The universe is not so badly designed!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Attention on deck, Ensign Picard!" :quote/source "Q"}
     {:quote/quote "Q." :quote/source "Picard"}
     {:quote/quote "That's Captain Q to you, Young man!" :quote/source "Q"}
     {:quote/quote "You cheated, Picard? I'm impressed!" :quote/source "Q"}
     {:quote/quote "There are many parts of my youth that I'm not proud of. There were loose threads - untidy parts of me that I would like to remove. But when I pulled on one of those threads, it unraveled the tapestry of my life." :quote/source "Jean-Luc Picard"}
     {:quote/quote "[Dressed as a delivery boy] Flowers! Is there a John Luck Pickird here?" :quote/source "Q"}
     {:quote/quote "There is no room in my heart for shame." :quote/source "Worf"}
     {:quote/quote "I give you one last chance to accept our way of life." :quote/source "Tokath"}
     {:quote/quote "Those are eloquent words, Tokath, but the truth is, I am being executed because I brought something dangerous to your young people-- knowledge." :quote/source "Worf"}
     {:quote/quote "Knowledge of their origins, knowledge of the real reasons you are here in this camp." :quote/source "Worf"}
     {:quote/quote "The truth is a threat to you." :quote/source "Worf"}
     {:quote/quote "Enough!" :quote/source "Tokath"}
     {:quote/quote "My upper spinal support is a poly-alloy designed to withstand extreme stress. My skull is composed of cortenide and duranium." :quote/source "Data"}
     {:quote/quote "You are attempting to bribe me." :quote/source "Data"}
     {:quote/quote "N-Not at all." :quote/source "Nu'Daq"}
     {:quote/quote "You suggested a plan that would work to your advantage. One that I would be capable of executing. You then implied a reward. Clearly you were..." :quote/source "Data"}
     {:quote/quote "Commander..., never mind." :quote/source "Nu'Daq"}
     {:quote/quote "What?! You incompetent  toppa' " :quote/source "Nu'Daq"}
     {:quote/quote "He's right. For all we know, this might just be a recipe for biscuits!" :quote/source "Gul Ocett"}
     {:quote/quote "BISCUITS?! If that is what you believe, then go back to Cardassia. I will send you my mother's recipe." :quote/source "Nu'Daq"}
     {:quote/quote "You're wondering who we are. Why we have done this. I stand before you, the image of a being from so long ago. Life evolved on my planet, before all others in this part of the galaxy. We left our world, explored the stars, and found none like ourselves. Our civilization thrived for ages. But what is the life of one race, compared to the vast stretches of cosmic time? We knew that one day we would be gone, and that nothing of us would survive. So we left you. Our scientists seeded the primordial oceans of many worlds, where life was in its infancy. The seed codes directed your evolution towards a physical form resembling ours. This body you see before you, which is of course shaped as yours is shaped, for you are the end result. The seed codes also contained this message, which was scattered in fragments on many different worlds. It was our hope that you would have to come together in fellowship and companionship to hear this message. And if you can see and hear me, our hope has been fulfilled. You are a monument, not to our greatness, but to our existence. That was our wish. That you too, would know life, and would keep alive our memory. There is something of us in each of you. And so, something of you in each other. Remember us." :quote/source "Ancient Humanoid"}
     {:quote/quote "That's all?! If she were not dead, I would kill her." :quote/source "Nu'Daq"}
     {:quote/quote "The very notion that a Cardassian could have anything in common with a Klingon... it turns my stomach." :quote/source "Gul Ocett"}
     {:quote/quote "Dr. Fesbinder gave an hour long dissertation on the ionization of warp nacelles before he realized that the topic was supposed to be psychology." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Why didn't anybody tell him?" :quote/source "Geordi La Forge"}
     {:quote/quote "There was no opportunity. There was no pause. (droning) He-just-kept-talking-in-one-long-incredibly-unbroken-sentence, moving-from-topic-to-topic-so-that-no-one-had-the-chance-to-interrupt, it-was-really-quite-hypnotic." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Captain, incoming message from the New Berlin Colony. They have cancelled their distress call. Evidently a Ferengi trading ship entered their system and someone panicked." :quote/source "Worf"}
     {:quote/quote "Third time today. Stand down Red alert. Reduce speed. Return to our patrol route." :quote/source "Riker"}
     {:quote/quote "Mister Worf, acknowledge the signal from New Berlin, transmit another copy of Starfleet's ship-recognition protocols - and tell them to read it this time." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Aye, sir." :quote/source "Worf"}
     {:quote/quote "They were fast, aggressive, almost vicious. It was more like fighting Klingons than... [realizes] ...Borg. [to Worf] No offense." :quote/source "Riker"}
     {:quote/quote "None taken." :quote/source "Worf"}
     {:quote/quote "May I ask a personal favor?" :quote/source "Data"}
     {:quote/quote "Yes?" :quote/source "Worf"}
     {:quote/quote "Will you take care of Spot for me?" :quote/source "Data"}
     {:quote/quote "Your animal?" :quote/source "Worf"}
     {:quote/quote "I am concerned that if I have another waking dream, I may injure him." :quote/source "Data"}
     {:quote/quote "Of course. Spot, come here." :quote/source "Worf"}
     {:quote/quote "Unlike a canine, Spot will not respond to verbal commands." :quote/source "Data"}
     {:quote/quote "Goodbye, Spot. He will need to be fed once a day. He prefers feline supplement one-twenty-five." :quote/source "Data"}
     {:quote/quote "I understand." :quote/source "Worf"}
     {:quote/quote "And he will require water. And you must also provide him with a sand box. And you must talk to him. Tell him he is a pretty cat, and a good cat, and—" :quote/source "Data"}
     {:quote/quote "I will feed him." :quote/source "Worf"}
     {:quote/quote "Perhaps that will be enough." :quote/source "Data"}
     {:quote/quote "What type of cake is that?" :quote/source "Picard"}
     {:quote/quote "It is a cellular peptide cake." :quote/source "Data"}
     {:quote/quote "[With mouth full] With mint frosting." :quote/source "Worf"}
     {:quote/quote "That's it! I can see that diplomacy is not going to get us anywhere today, and I do not have time to negotiate. So let's put all of our cards on the table. [to Lorin] You're concerned the Kes are going to be admitted to the Federation." :quote/source "Riker"}
     {:quote/quote "Correct." :quote/source "Lorin"}
     {:quote/quote "As First Officer of the Enterprise, I think I can promise you it is not going to happen. The Kes will be denied membership." :quote/source "Riker"}
     {:quote/quote "You have no authority to make that decision! Despite whatever games you played with the Prytt when you arrived, we still plan to take our petition directly to the Federation Council! They'll listen-!" :quote/source "Mauric"}
     {:quote/quote '"Kesprytt, a deeply troubled world with social, political, and military problems that they have yet to resolve. The Kes, while a friendly and democratic people, are driven by suspicion, deviousness, and paranoia. It is the opinion of this officer that they are not ready for membership.'' :quote/source 'Riker: They will also listen to the reports of the Captain of the Enterprise and his First Officer! And I can tell you right now the First Officer's report will go something like this"}
     {:quote/quote "This is down. Down is good." :quote/source "Data"}
     {:quote/quote "This is up. Up is no." :quote/source "Data"}
     {:quote/quote "I suppose I must accept that possibility. It may be that Spot lacks the intelligence necessary to learn the appropriate responses to my commands." :quote/source "Data"}
     {:quote/quote "Mmm?" :quote/source "Data"}
     {:quote/quote "Ah." :quote/source "Data"}
     {:quote/quote "I don't know about Spot, but your training is coming along just fine. Let's go." :quote/source "Geordi La Forge"}
     {:quote/quote "Captain, we're receiving two-hundred and eighty-five thousand hails." :quote/source "Wesley Crusher"}
     {:quote/quote "Well, they seem to have a somewhat exaggerated opinion of me." :quote/source "Picard"}
     {:quote/quote "Oh, I don't know, I think the resemblance is rather striking. Wouldn't you agree, Number One?" :quote/source "Riker"}
     {:quote/quote "Isn't there something else you have to do?" :quote/source "Picard"}
     {:quote/quote "I'll be on the bridge!" :quote/source "Riker"}
     {:quote/quote "[over ship's intercom] 'To all Starfleet personel, this is the Captain. It is my sad duty to inform you that a member of the crew, Ensign Sito Jaxa has been lost in the line of duty. She was the finest example of a Starfleet officer and a young woman of remarkable courage and strength of character. Her loss will be deeply felt by all who knew her. Picard out.'' :quote/source 'Picard"}
     {:quote/quote "Geordi,…what…does it feel like…when a person is losing his mind?" :quote/source "Data"}
     {:quote/quote "Have I been dreaming again?" :quote/source "Data"}
     {:quote/quote "Do not approach me unannounced—especially while I am eating." :quote/source "Worf"}
     {:quote/quote "you'll never look at your hairline the same way again." :quote/source "Jean-Luc Picard: One thing is clear"}
     {:quote/quote "Oh, you'd like me to connect the dots for you, lead you from A to B to C, so that your puny mind could comprehend. How boring." :quote/source "Q"}
     {:quote/quote "Goodbye, Jean-Luc. I'm gonna miss you…you had such potential. But then again, all good things must come to an end…" :quote/source "Q"}
     {:quote/quote "[In distance] Captain Picard to the bridge. Captain we've got a problem with the warp core, or the phase inducers, or some other damn thing." :quote/source "Geordi"}
     {:quote/quote "Geordi!" :quote/source "Picard"}
     {:quote/quote "If you're really his friend you'll get him to take that gray out of his hair." :quote/source "Jessel"}
     {:quote/quote "Jessel..." :quote/source "Data"}
     {:quote/quote "Looks like a bloody skunk." :quote/source "Jessel"}
     {:quote/quote "She can be frightfull trying at times, but, she does make me laugh." :quote/source "Data"}
     {:quote/quote "Data, what is it with the hair anyway?" :quote/source "Geordi"}
     {:quote/quote "I've found that a touch of gray adds an air of distinction." :quote/source "Data"}
     {:quote/quote "You say this is Earl Grey, I'd swear that it was Darjeeling." :quote/source "Picard"}
     {:quote/quote "Hmm." :quote/source "Jessel"}
     {:quote/quote "Time may be eternal, Captain, but our patience is not. It's time to put an end to your little 'trek' through the stars; make room for other more…worthy species.' :quote/source 'Q"}
     {:quote/quote "You're...going to deny us travel through space?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "[to crowd] He doesn't understand! [to Picard] You obtuse piece of flotsam, you are to be denied existence. Humanity's fate has been sealed, you will be destroyed." :quote/source "Q"}
     {:quote/quote "[Appearing aged, raises an ear trumpet] Eh?  What did she say, sonny? I couldn't quite hear her." :quote/source "Q"}
     {:quote/quote "Q! What is going on? Where is the anomaly?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Where's your mommy? Well, I don't know!" :quote/source "Q"}
     {:quote/quote "You just don't get it, do you, Jean-Luc? The trial never ends. We wanted to see if you had the ability to expand your mind and your horizons. And for one brief moment, you did." :quote/source "Q"}
     {:quote/quote "When I realized the paradox." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Exactly. For that one fraction of a second, you were open to options you had never considered. That is the exploration that awaits you. Not mapping stars and studying nebulae, but charting the unknowable possibilities of existence." :quote/source "Q"}
     {:quote/quote "Q, what is it you're trying to tell me?" :quote/source "Jean-Luc Picard"}
     {:quote/quote "You'll find out. In any case, I'll be watching. And if you're very lucky I'll drop by to say hello from time to time. See you...out there." :quote/source "Q"}
     {:quote/quote "I should have done this a long time ago." :quote/source "Jean-Luc Picard"}
     {:quote/quote "You were always welcome." :quote/source "Deanna Troi"}
     {:quote/quote "So, five-card stud, nothing wild... and the sky's the limit." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Those who rely on luck... never win the battle." :quote/source "Worf"}
     {:quote/quote "Space, the final frontier. These are the voyages of the starship Enterprise. Its continuing mission-- to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no one has gone before. [Introduction to each Next Generation episode.]" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Make it so!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Tea. Earl grey. Hot." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Engage!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Data, later." :quote/source "Jean-Luc Picard"}
     {:quote/quote "Shut up, Wesley!" :quote/source "Jean-Luc Picard"}
     {:quote/quote "Today is a good day to die!" :quote/source "Worf"}
     {:quote/quote "However..." :quote/source "Data"}
     {:quote/quote "Theoretically, it is possible..." :quote/source "Data"}
     {:quote/quote "Thank you, Mr. Data." :quote/source "Picard"}]))
